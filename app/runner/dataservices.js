app.config(['dataServiceProvider', function(dataServiceProvider){
    //Set the Base URL Using the App Config String (config.js)
    dataServiceProvider.config( api_url );
}]);

app.provider('dataService', function(){

    var baseUrl = '';

    this.config = function(base_url){
        baseUrl = base_url;
    };

    this.$get = [ '$http', '$log', function( $http, $log ){

        var objDataService = {};

        //Request Method: POST
        objDataService.POSTRequest = function(urlParams, dataParams){
            return $http({
                url : baseUrl + urlParams,
                method : 'POST',
                data : dataParams,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        };  

        //Request Method: POST
        objDataService.GetExcel = function(urlParams, dataParams){
            return $http({
                url : baseUrl + urlParams,
                method : 'POST',
                data : dataParams,
                headers: {'Content-Type': 'application/vnd-ms-excel'}
            });
        }; 

        //Request Method: Upload
        objDataService.UploadFile = function(urlParams, fileData){
            return $http({
                method: 'POST',
                url: baseUrl + urlParams,
                data: fileData,
                headers: {'Content-Type': undefined},
            });
        };

        //Request Method: GET
        objDataService.GETRequest = function(urlParams){
            return $http({
                url : baseUrl + urlParams,
                method : 'GET'
            });
        };

        return objDataService;

    }];
});