app.controller('mainController', ['$scope', '$timeout', '$interval', 'dataService', '$rootScope', function ($scope, $timeout, $interval, dataService, $rootScope) {
    $scope.appData = {};

    //$('#preloader').show('fast');

    dataService.GETRequest('q=is-login').then(function (response) {
        //console.log(response.data);
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username;
            //$scope.appData.name = response.data.dataAuth.name;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function () {  
                $scope.getData();            
                $('#main-content').show();  
            }, 0);
        } else {
            location.href = 'login.html';
        }
    });

    $scope.getData = function () {
        var urlParams = 'q=api&m=app&a=get-dashboard';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        //console.log(dataParams);                                        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            //console.log(response.data.dataChart.chartlocday);
            //console.log(response.data.dataChart.chartlocday);
            //$scope.objectArray = json.map((each)=>{return JSON.parse(each)});
            //console.log(objectArray);
            if (response.data.status == 'OK') {
                $timeout(function () { 
                    
                    $scope.appData.numJukir = response.data.dataDashboard.numJukir;
                    $scope.appData.numLokasi = response.data.dataDashboard.numLokasi;
                    $scope.appData.numIncomeHariIni = response.data.dataDashboard.numIncomeHariIni;
                    $scope.appData.numIncomeBulanIni = response.data.dataDashboard.numIncomeBulanIni;
                    $scope.createPieChartLocation(response.data.dataChart.data);//response.data.dataChart.label, response.data.dataChart.data      
                    $scope.createLineChartLokasiHari(response.data.dataChart.chartlocday);
                    $scope.createLineChartLokasiBulan(response.data.dataChart.chartlocmonth);//response.data.dataChart.chartlocday
                    //$scope.createLineChartLokasiHari();
                    $scope.createPieChartMarital(response.data.dataChart.chartRideMonth);
                    $scope.mapGoogle(response.data.dataChart.chartMap);
                    
                }, 0);  
            } else {
                $rootScope.messagePopUp('error', response.data.message);
                if(response.data.message == 'Oops! Authentication failed.') {      
                    location.href = 'login.html';
                }
            }
        });
    }; 
    
    $scope.mapGoogle = function(ChartDataMarker){ //ChartDataMarker     
        google.charts.load("current", {
            "packages":["map"],
            // Note: you will need to get a mapsApiKey for your project.
            // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
            "mapsApiKey": "AIzaSyDM47HM8U99x71N3AWSEzIUOKplTd0T6po"//"AIzaSyDOvbTAxsGpbk-xctxHEWyrat1NBoRUQw8"
        });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable(ChartDataMarker); //ChartDataMarker

            var map = new google.visualization.Map(document.getElementById('map_div'));
            map.draw(data, {
              showTooltip: true,
              showInfoWindow: false,
              zoomLevel: 14,
              mapType: 'normal',
              icons: {
                default: {
                  normal: 'https://siprima.signa.id/assets/img/survey-location4.png',
                  selected: 'https://siprima.signa.id/assets/img/survey-location4.png'
                }
              }
            });
        }
    };   
    
    $scope.mapGoogleGo = function(ChartDataMarker){      
        google.charts.load("current", {
            "packages":["map"],
            // Note: you will need to get a mapsApiKey for your project.
            // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
            "mapsApiKey": "AIzaSyDM47HM8U99x71N3AWSEzIUOKplTd0T6po"//"AIzaSyDOvbTAxsGpbk-xctxHEWyrat1NBoRUQw8"
        });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable(ChartDataMarker);

            var map = new google.visualization.Map(document.getElementById('map_div'));
            map.draw(data, {
              showTooltip: true,
              showInfoWindow: false,
              zoomLevel: 14,
              mapType: 'normal',
              icons: {
                default: {
                  normal: 'https://siprima.signa.id/assets/img/survey-location4.png',
                  selected: 'https://siprima.signa.id/assets/img/survey-location4.png'
                }
              }
            });
        }
    };
    
    $scope.createPieChartLocation = function(ChartDataLokasi) {    //ChartDataLokasi  
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(ChartDataLokasi); //ChartDataLokasi

        var options = {
          title: 'Parking Location',
          width: 475,
          height: 500,
          pieHole: 0.4
        };

        var chart = new google.visualization.PieChart(document.getElementById('locationchart'));

        chart.draw(data, options);
      }
    };
    
    $scope.createLineChartLokasiBulan = function(chartDataUsia) {      //ChartDataJenisKelamin
      google.charts.load('current', {'packages':['corechart']});//corechart
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(chartDataUsia);//ChartDataJenisKelamin

        var options = {
          title: 'Monthly Revenue',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('sexchart'));

        chart.draw(data, options);
      }
    };
    
    $scope.createLineChartLokasiHari = function(chartDataUsia) {      //ChartDataJenisKelamin
      google.charts.load('current', {'packages':['corechart']});//corechart
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(chartDataUsia);//ChartDataJenisKelamin

        var options = {
          title: 'Daily Revenue',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('agechart'));

        chart.draw(data, options);
      }
    };
    
    $scope.createLineChartLokasiNotYet = function() {      //chartDataUsia ### chartlabel,chartDataUsia
      google.charts.load('current', {'packages':['line']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        //var kpala= chartDataUsia[0];
//        kpala.foreach(item,index){
//            console.log("isi : "+item);
//        }
        //console.log(chartDataUsia);
        var data = new google.visualization.DataTable();//arrayToDataTable(chartDataUsia)
        data.addColumn('number', 'Date');
        data.addColumn('number', 'Guardians of the Galaxy');
        data.addColumn('number', 'The Avengers');
        data.addColumn('number', 'Transformers: Age of Extinction');

        data.addRows([
            [1,  37.8, 80.8, 41.8],
            [2,  30.9, 69.5, 32.4],
            [3,  25.4,   57, 25.7],
            [4,  11.7, 18.8, 10.5],
            [5,  11.9, 17.6, 10.4],
            [6,   8.8, 13.6,  7.7],
            [7,   7.6, 12.3,  9.6],
            [8,  12.3, 29.2, 10.6],
            [9,  16.9, 42.9, 14.8],
            [10, 12.8, 30.9, 11.6],
            [11,  5.3,  7.9,  4.7],
            [12,  6.6,  8.4,  5.2],
            [13,  4.8,  6.3,  3.6],
            [14,  4.2,  6.2,  3.4]
          ]);
        var options = {
          chart: {
          title: 'Box Office Earnings in First Two Weeks of Opening',
          subtitle: 'in millions of dollars (USD)'
          },
          width: 475,
          height: 405,
          axes: {
          x: {
            0: {side: 'top'}
          }
        }
        };

        //var chart = new google.visualization.PieChart(document.getElementById('agechart'));
        var chart = new google.charts.Line((document.getElementById('agechart')));

        chart.draw(data, google.charts.Line.convertOptions(options));
      }
    };   
    
    $scope.createPieChartMarital = function(ChartDataMarital) {      //ChartDataMarital
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(ChartDataMarital);//ChartDataMarital

        var options = {
          chart: {
            title: 'Kendaraan Penyumbang Revenue',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          width: 475,
          height: 495,
          bars: 'vertical' // Required for Material Bar Charts.
        };//horizontal

        var chart = new google.charts.Bar(document.getElementById('maritalchart'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    };  
    
    $scope.createPieChartSpouseJob = function(ChartDataPekerjaanPsg) {      
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(ChartDataPekerjaanPsg);

        var options = {
          title: 'Pekerjaan Pasangan Responden',
          pieHole: 0.4
        };

        var chart = new google.visualization.PieChart(document.getElementById('jobspousechart'));

        chart.draw(data, options);
      }
    };
    
    $scope.createPieChartEdu = function(ChartDataPendidikan) {      
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(ChartDataPendidikan);

        var options = {
          title: 'Pendidikan Responden',     
          pieHole: 0.4
        };

        var chart = new google.visualization.PieChart(document.getElementById('educhart'));

        chart.draw(data, options);
      }
    };
    
    $scope.createPieChartSpouseEdu = function(ChartDataEduPsg) {      
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable(ChartDataEduPsg);

        var options = {
          title: 'Pendidikan Pasangan Responden',
          pieHole: 0.4
        };

        var chart = new google.visualization.PieChart(document.getElementById('spouseeduchart'));

        chart.draw(data, options);
      }
    };    

}]);

app.controller('welcomeController', ['$scope', '$timeout', '$interval', 'dataService', '$rootScope', function ($scope, $timeout, $interval, dataService, $rootScope) {
    $scope.appData = {};
    $('#preloader').hide('fast');

    dataService.GETRequest('q=is-login').then(function (response) {
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username; 
            $scope.appData.name = response.data.dataAuth.name;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
        } else {
            location.href = 'login.html';
        }
    });

}]);
                
app.controller('datatransaksiController', ['$scope', '$timeout', 'dataService', '$rootScope', '$window', '$location' , function ($scope, $timeout, dataService, $rootScope, $window, $location) {

    $scope.appData = {};
    $scope.formData = {};   
    $scope.formData.showExportButton = false;

    // $('#preloader').show();

    dataService.GETRequest('q=is-login').then(function (response) {
        //console.log(response);
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function () {
                $scope.getData();
            }, 0);
        }
    });  
    
    $scope.getData = function () {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=trx&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if ($.fn.DataTable.isDataTable('#tbl-data-table')) {
                $('#tbl-data-table').DataTable().destroy();
            }
            //console.log(response);
            if (response.data.status == 'OK') {
                $scope.dataTrxDaily = {};
                $scope.dataTrxDaily = response.data.dataRecords;
                $timeout(function () {
                    $("#tbl-data-table").DataTable({
                        "dom": "Bfrtip",
                        "buttons": [
                            "copy", "csv", "excel", "pdf", "print"
                        ],
                        "destroy": true,                        
                        "initComplete": function () {                            
                            //$('#preloader').hide('fast');
                        }
                    });
                    //$(document).ready(function() {
//                        $('#tbl-data-table').DataTable( {
//                            dom: 'Bfrtip',
//                            buttons: [
//                                'copy', 'csv', 'excel', 'pdf', 'print'
//                            ]
//                        } );
//                    } );
                }, 0);
            } else {
                $scope.dataTrxDaily = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function () {
                    if(response.data.message == 'Oops! Authentication failed.') {      
                        location.href = 'login.html';
                    }
                    $("#tbl-data-table").DataTable({
                        "destroy": true,
                        "initComplete": function () {
                            //$('#preloader').hide('fast');
                        }
                    });
                }, 0);
            }
        });
        
        //####
        var urlParams2 = 'q=api&m=trx&a=get-form-data';
        dataService.POSTRequest(urlParams2, dataParams).then(function (response) {
            //alert('testing');
            //console.log(JSON.stringify(response.data));
            if (response.data.status == 'OK') {
                $scope.formData.dataJukir = response.data.dataJukir;
                $scope.formData.dataLocation = response.data.dataLocation;
                $scope.formData.dataTarif = response.data.dataTarif;
                //$scope.formData.dataHari = response.data.dataHari;
                //if(modal_selector != null) {
//                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                    $('#datepicker').datepicker({
                      autoclose: true,
                      format: 'yyyy-mm-dd'
                    });
                //} 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };   

    $scope.getDetailData = function (trx_id) {
        //alert(trx_id+"OK");
        //$scope.profileParams.isEditProfile = false;  
        var urlParams = 'q=api&m=trx&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : trx_id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if ($.fn.DataTable.isDataTable('#tbl-data-table')) {
                $('#tbl-data-table').DataTable().destroy();
            }
            console.log(response.data.dataRecords);
            if (response.data.status == 'OK') {
                $scope.dataTrxDet = response.data.dataRecords; 
                //$scope.dataSosialEkonomi = response.data.dataSosialEkonomi; 
//                $scope.dataKualitasHidup = response.data.dataKualitasHidup; 
//                $scope.dataPrestasiProgramA = response.data.dataPrestasiProgramA; 
//                $scope.dataPrestasiProgramB = response.data.dataPrestasiProgramB; 
//                $scope.dataPrestasiProgramC = response.data.dataPrestasiProgramC; 
//                $scope.profileParams.isEditProfile = true;
                $('#modal-detail-data').modal('toggle');   
            } else {
                $scope.dataUserProfile = {};
                $rootScope.messagePopUp('error', response.data.message);  
            }
        });
    };
    
    $scope.getDataJukir = function() {
        var urlParams = 'q=api&m=laporan&a=get-sum-daily';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataRekapHarianList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataLaporanList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });   
        //####
        var urlParams2 = 'q=api&m=laporan&a=get-form-data';
        dataService.POSTRequest(urlParams2, dataParams).then(function (response) {
            //alert('testing');
            //console.log(JSON.stringify(response.data));
            if (response.data.status == 'OK') {
                $scope.formData.dataKampus = response.data.dataKampus;
                $scope.formData.dataTingkat = response.data.dataTingkat;
                $scope.formData.dataHalaqah = response.data.dataHalaqah;
                $scope.formData.dataHari = response.data.dataHari;
                //if(modal_selector != null) {
//                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                    $('#datepicker').datepicker({
                      autoclose: true,
                      format: 'yyyy-mm-dd'
                    });
                //} 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });  
    };
    
    $scope.searchData = function() {
        //$scope.dataUser = {};
        //alert('kamp : '+$scope.formData.SelKampus+'=> Tingkat : '+$scope.formData.SelTingkat+'=> Halaqah : '+$scope.formData.SelHalaqah);
//        return false;
        //$scope.formData.showExportButton = true;        
        if($scope.formData.SelHari === 'undefined'){$scope.formData.SelHari="";}
        if($scope.formData.SelJukir === 'undefined'){$scope.formData.SelJukir="";}
        if($scope.formData.SelTarif === 'undefined'){$scope.formData.SelTarif="";}
        //if($scope.formData.SelHalaqah === 'undefined'){$scope.formData.SelHalaqah="";}
//        if($scope.formData.SelAchieve === 'undefined'){$scope.formData.SelAchieve="";}
        //alert($scope.formData.SelHari+$scope.formData.SelJukir+$scope.formData.SelTarif);
//        return false;
        var urlParams = 'q=api&m=trx&a=get-trx-daily';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,            
            "hari" : $scope.formData.SelHari,
            "id_jukir" : $scope.formData.SelJukir,
            "trx_denom" : $scope.formData.SelTarif            
            //"halaqah" : $scope.formData.SelHalaqah,
//            "target" : $scope.formData.SelAchieve
        };
        //console.log(JSON.stringify(dataParams));
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //alert('testing');
            //console.log(JSON.stringify(response.data));
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.numRecords > 0){
                $scope.formData.showExportButton = true;
            }else{
                $scope.formData.showExportButton = false;
            }            
            if(response.data.status == 'OK') {
                $scope.dataTrxDaily = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "dom": "Bfrtip",
                        "buttons": [
                            "copy", "csv", "excel", "pdf", "print"
                        ],
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } 
            else {
                $scope.dataTrxDaily = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });
    }; 
    
    $scope.downloadData = function() {
        //$scope.dataUser = {};        
        if($scope.formData.SelHari === 'undefined'){$scope.formData.SelHari="";}
        if($scope.formData.SelJukir === 'undefined'){$scope.formData.SelJukir="";}
        if($scope.formData.SelTarif === 'undefined'){$scope.formData.SelTarif="";}
        //if($scope.formData.SelHalaqah === 'undefined'){$scope.formData.SelHalaqah="";}
//        if($scope.formData.SelAchieve === 'undefined'){$scope.formData.SelAchieve="";}
        //alert($scope.formData.SelHari+$scope.formData.SelJukir+$scope.formData.SelTarif);
//        return false;
        var urlParams = 'https://spark.signa.id/app/data/api/module/download/index.php?m=trx-daily&tanggal='+ $scope.formData.SelHari +'&jukir='+ $scope.formData.SelJukir +'&tarif='+$scope.formData.SelTarif;//q=api&m=trx&a=get-trx-daily
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,            
            "hari" : $scope.formData.SelHari,
            "id_jukir" : $scope.formData.SelJukir,
            "trx_denom" : $scope.formData.SelTarif            
            //"halaqah" : $scope.formData.SelHalaqah,
//            "target" : $scope.formData.SelAchieve
        };
        alert(urlParams);
        $window.open('https://spark.signa.id/app/data/api/module/download/index.php?m=trx-daily&tanggal='+ $scope.formData.SelHari +'&jukir='+ $scope.formData.SelJukir +'&tarif='+$scope.formData.SelTarif,'_blank');        
        //$windows.location.href = urlParams;
        return false;
        alert('Hari : '+$scope.formData.SelHari+'=> Jukir : '+$scope.formData.SelJukir+'=> Tarif : '+$scope.formData.SelTarif);
        return false;
        //console.log(JSON.stringify(dataParams));
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //alert('testing');
            //console.log(JSON.stringify(response.data));
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataTrxDaily = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } 
            else {
                $scope.dataTrxDaily = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });
    }; 
    
    $scope.detailDataJukir = function(nis,tanggal) {
        //alert('kamp : '+nis+'=> Tingkat : '+tanggal);
        //return false;        
        //alert();
//        return false;
        var urlParams = 'q=api&m=laporan&a=get-det-daily';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,            
            "siswa_id" : nis,
            "tanggal" : tanggal            
        };
        //console.log(JSON.stringify(dataParams));return false;
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //alert('testing');
            //console.log(response);return false;//.data
            //console.log(JSON.stringify(response.data));return false;//.data
            if ( $.fn.DataTable.isDataTable('#tbl-detail-table') ) {
                $('#tbl-detail-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataRekapHarianDet = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-detail-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } 
            else {
                $scope.dataRekapHarianDet = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-detail-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });
    };    

}]);  

app.controller('vehicleController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtVehicleType = '';
    //$scope.formData.TxtAlamat = '';
    //$scope.formData.TxtEmailParent = '';
//    $scope.formData.TxtPhoneParent = '';
//    $scope.formData.SelSex = '';
//    $scope.formData.SelKampus = '';
//    $scope.formData.SelTingkat = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=vehicle&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataVehicleList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataVehicleList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=vehicle&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        /*
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                //$scope.formData.dataKampus = response.data.dataKampus;
//                $scope.formData.dataTingkat = response.data.dataTingkat;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });*/
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtVehicleType = '';
        //$scope.formData.TxtAlamat = '';
//        $scope.formData.TxtEmailParent = '';
//        $scope.formData.TxtPhoneParent = '';
//        $scope.formData.SelSex = '';
//        $scope.formData.SelKampus = '';
//        $scope.formData.SelTingkat = '';
        //$scope.formData.SelClient = '';
        //$timeout(function(){    
//            $('.select2').select2();
//        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Vehicle';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=vehicle&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"alamat" : $scope.formData.TxtAlamat,
            "vehicle_type" : $scope.formData.TxtVehicleType,
            //"alamat" : $scope.formData.TxtAlamat,
//            "phone_orangtua" : $scope.formData.TxtPhoneParent,
//            "jenis_kelamin" : $scope.formData.SelSex,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtVehicleType = data.vehicle_type;
        //$scope.formData.TxtAlamat = data.alamat;
//        $scope.formData.TxtPhoneParent = data.phone_orangtua;
//        $scope.formData.SelSex = data.jenis_kelamin;
//        $scope.formData.SelKampus = data.kampus_id;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_vehicle_type;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Vehicle';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=vehicle&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "vehicle_type" : $scope.formData.TxtVehicleType,
            //"alamat" : $scope.formData.TxtAlamat,
            //"phone_orangtua" : $scope.formData.TxtPhoneParent,
//            "jenis_kelamin" : $scope.formData.SelSex,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=vehicle&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('tarifController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtTarifName = '';
    $scope.formData.TxtDenom = '';
    //$scope.formData.TxtEmailParent = '';
//    $scope.formData.TxtPhoneParent = '';
    $scope.formData.SelVehicle = '';
    $scope.formData.SelLocation = '';
//    $scope.formData.SelTingkat = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=tarif&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataTarifList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataTarifList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=tarif&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            //console.log(response.data);
            if (response.data.status == 'OK') {
                $scope.formData.dataVehicle = response.data.dataVehicle;
                $scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtTarifName = '';
        $scope.formData.TxtDenom = '';
//        $scope.formData.TxtEmailParent = '';
//        $scope.formData.TxtPhoneParent = '';
        $scope.formData.SelVehicle = '';
        $scope.formData.SelLocation = '';
//        $scope.formData.SelTingkat = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Tarif';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=tarif&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"alamat" : $scope.formData.TxtAlamat,
            "tarif_name" : $scope.formData.TxtTarifName,
            "tarif_denom" : $scope.formData.TxtDenom,
            "vehicle_type" : $scope.formData.SelVehicle,
            "parking_location" : $scope.formData.SelLocation,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtTarifName = data.tarif_name;
        $scope.formData.TxtDenom = data.tarif_denom;
//        $scope.formData.TxtPhoneParent = data.phone_orangtua;
        $scope.formData.SelVehicle = data.id_vehicle_type;
        $scope.formData.SelLocation = data.id_parking_location;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_tarif;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Tarif';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=tarif&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "tarif_name" : $scope.formData.TxtTarifName,
            "tarif_denom" : $scope.formData.TxtDenom,
            //"phone_orangtua" : $scope.formData.TxtPhoneParent,
            "vehicle_type" : $scope.formData.SelVehicle,
            "parking_location" : $scope.formData.SelLocation,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=tarif&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('deviceController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtDeviceType = '';
    $scope.formData.TxtDeviceCode = '';
    $scope.formData.TxtDeviceUuid = '';
//    $scope.formData.TxtPhoneParent = '';
    $scope.formData.SelJukir = '';
    //$scope.formData.SelLocation = '';
//    $scope.formData.SelTingkat = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=device&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataDeviceList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataDeviceList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=device&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            //console.log(response.data);
            if (response.data.status == 'OK') {
                $scope.formData.dataJukir = response.data.dataJukir;
                //$scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtDeviceType = '';
        $scope.formData.TxtDeviceCode = '';
        $scope.formData.TxtDeviceUuid = '';
//        $scope.formData.TxtPhoneParent = '';
        $scope.formData.SelJukir = '';
        //$scope.formData.SelLocation = '';
//        $scope.formData.SelTingkat = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Device';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=device&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "device_uuid" : $scope.formData.TxtDeviceUuid,
            "device_type" : $scope.formData.TxtDeviceType,
            "device_code" : $scope.formData.TxtDeviceCode,
            "jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtDeviceType = data.device_type;
        $scope.formData.TxtDeviceCode = data.device_code;
        $scope.formData.TxtDeviceUuid = data.device_uuid;
        $scope.formData.SelJukir = data.id_jukir;
        //$scope.formData.SelLocation = data.id_parking_location;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_device;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Device';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=device&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "device_type" : $scope.formData.TxtDeviceType,
            "device_code" : $scope.formData.TxtDeviceCode,
            "device_uuid" : $scope.formData.TxtDeviceUuid,
            "jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=device&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('clientController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNama = '';
    $scope.formData.TxtKode = '';
    $scope.formData.TxtAlamat = '';
    $scope.formData.TxtPhone = '';
    $scope.formData.TxtEmail = '';
    $scope.formData.TxtLongitude = '';
    $scope.formData.TxtLatitude = '';
    //$scope.formData.SelJukir = '';
    //$scope.formData.SelLocation = '';
//    $scope.formData.SelTingkat = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=client&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataClientList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });   
                }, 0);
            } else {
                $scope.dataClientList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=client&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        /*dataService.POSTRequest(urlParams, dataParams).then(function (response) {            
            if (response.data.status == 'OK') {
                $scope.formData.dataJukir = response.data.dataJukir;
                //$scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });*/
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNama = '';
        $scope.formData.TxtKode = '';
        $scope.formData.TxtAlamat = '';
        $scope.formData.TxtPhone = '';
        $scope.formData.TxtEmail = '';
        $scope.formData.TxtLongitude = '';
        $scope.formData.TxtLatitude = '';
        //$scope.formData.SelJukir = '';
        //$scope.formData.SelLocation = '';
//        $scope.formData.SelTingkat = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Client';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=client&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "client_name" : $scope.formData.TxtNama,
            "address" : $scope.formData.TxtAlamat,
            "phone" : $scope.formData.TxtPhone,
            "email" : $scope.formData.TxtEmail,
            "office_long" : $scope.formData.TxtLongitude,
            "office_lat" : $scope.formData.TxtLatitude,
            "client_code" : $scope.formData.TxtKode,
            //"client" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNama = data.client_name;
        $scope.formData.TxtKode = data.client_code;
        $scope.formData.TxtAlamat = data.address;
        $scope.formData.TxtPhone = data.phone;
        $scope.formData.TxtEmail = data.email;
        $scope.formData.TxtLongitude = data.office_long;
        $scope.formData.TxtLatitude = data.office_lat;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_client;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Client';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=client&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "client_name" : $scope.formData.TxtNama,
            "client_code" : $scope.formData.TxtKode,
            "address" : $scope.formData.TxtAlamat,
            "phone" : $scope.formData.TxtPhone,
            "email" : $scope.formData.TxtEmail,
            "office_long" : $scope.formData.TxtLongitude,
            "office_lat" : $scope.formData.TxtLatitude,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=client&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.callFormChangePass = function(id) {
        //alert(id);
        $scope.formData.formTitle = 'Change Password';          
        $scope.resetFormPass(id);  
        $scope.getFormData('modal-change-password');  
    };
    
    $scope.resetFormPass = function(id) {          
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;
        //$scope.formData.TxtNama = '';
//        $scope.formData.TxtPhone = '';
//        $scope.formData.TxtEmail = '';
        $scope.formData.DataPassID = id;
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtConfirm = '';
        //$scope.formData.SelClient = '';
        
    };
    
    $scope.changePass = function() {        
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=changePassOnly';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,            
                "newpass": $scope.formData.TxtPassword,                        
                "id" : $scope.formData.DataPassID
            };
            //console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });
        }else{
            alert("Password did not match"); 
        }             
    };


}]);

app.controller('adminController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNamaAdmin = '';
    //$scope.formData.TxtKode = '';
    //$scope.formData.SelLocation = '';
//    $scope.formData.SelTingkat = '';
    $scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=client-admin&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataAdminList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataAdminList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=client-admin&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {            
            if (response.data.status == 'OK') {
                $scope.formData.dataClient = response.data.dataClient;
                //$scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNamaAdmin = '';
        //$scope.formData.TxtKode = '';
//        $scope.formData.TxtAlamat = '';
//        $scope.formData.SelTingkat = '';
        $scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Admin';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=client-admin&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "admin_name" : $scope.formData.TxtNamaAdmin,
            //"address" : $scope.formData.TxtAlamat,
//            "phone" : $scope.formData.TxtPhone,
//            "email" : $scope.formData.TxtEmail,
//            "office_long" : $scope.formData.TxtLongitude,
//            "office_lat" : $scope.formData.TxtLatitude,
//            "client_code" : $scope.formData.TxtKode,
            "client_id" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNamaAdmin = data.admin_name;
        //$scope.formData.TxtKode = data.client_code;
//        $scope.formData.TxtAlamat = data.address;
//        $scope.formData.TxtPhone = data.phone;
//        $scope.formData.TxtEmail = data.email;
//        $scope.formData.TxtLongitude = data.office_long;
//        $scope.formData.TxtLatitude = data.office_lat;
        $scope.formData.SelClient = data.id_client;
        $scope.formData.DataID = data.id_client_admin;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Admin';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=client-admin&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "admin_name" : $scope.formData.TxtNamaAdmin,
            //"client_code" : $scope.formData.TxtKode,
//            "address" : $scope.formData.TxtAlamat,
//            "phone" : $scope.formData.TxtPhone,
//            "email" : $scope.formData.TxtEmail,
//            "office_long" : $scope.formData.TxtLongitude,
//            "office_lat" : $scope.formData.TxtLatitude,
            "client_id" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id,client_id) {
        //alert(id);
        var urlParams = 'q=api&m=client-admin&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "client_id": client_id,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.callFormChangePass = function(id) {
        //alert(id);
        $scope.formData.formTitle = 'Change Password';          
        $scope.resetFormPass(id);  
        $scope.getFormData('modal-change-password');  
    };
    
    $scope.resetFormPass = function(id) {          
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;
        //$scope.formData.TxtNama = '';
//        $scope.formData.TxtPhone = '';
//        $scope.formData.TxtEmail = '';
        $scope.formData.DataPassID = id;
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtConfirm = '';
        //$scope.formData.SelClient = '';
        
    };
    
    $scope.changePass = function() {        
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=changePassOnly';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,            
                "newpass": $scope.formData.TxtPassword,                        
                "id" : $scope.formData.DataPassID
            };
            //console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });
        }else{
            alert("Password did not match"); 
        }             
    };


}]);

app.controller('userController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNama = '';
    $scope.formData.TxtPhone = '';
    $scope.formData.TxtEmail = '';
    $scope.formData.TxtUsername = '';
    $scope.formData.TxtPassowrd = '';
    $scope.formData.TxtConfirm = '';
    $scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=user&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataUserList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataUserList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=user&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {            
            if (response.data.status == 'OK') {
                $scope.formData.dataClient = response.data.dataClient;
                //$scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNama = '';
        $scope.formData.TxtPhone = '';
        $scope.formData.TxtEmail = '';
        $scope.formData.TxtUsername = '';
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtConfirm = '';
        $scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah User';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=post';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,
                "name" : $scope.formData.TxtName,
                "username" : $scope.formData.TxtUsername,
                "pass" : $scope.formData.TxtPassword,                
                "phone" : $scope.formData.TxtPhone,
                "email" : $scope.formData.TxtEmail,
    //            "client_code" : $scope.formData.TxtKode,
                "client_id" : $scope.formData.SelClient
            };
            console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();        
                    $scope.resetFormData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });    
        }else{
            alert("Password did not match");
        }
             
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtName = data.name;
        $scope.formData.TxtUsername = data.username;
        $scope.formData.TxtPassword = data.passwd;
        $scope.formData.TxtConfirm = data.passwd;
        $scope.formData.TxtEmail = data.email;
        $scope.formData.TxtPhone = data.phone;
//        $scope.formData.TxtLatitude = data.office_lat;
        $scope.formData.SelClient = data.id_client;
        $scope.formData.DataID = data.id_user;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data User';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=put';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,   
                //"nis" : $scope.formData.TxtNIS,
                "name" : $scope.formData.TxtName,
                "username" : $scope.formData.TxtUsername,
                "pass" : $scope.formData.TxtPassword,
                //"confirm_pass" : $scope.formData.TxtConfirm,
                "email" : $scope.formData.TxtEmail,
                "phone" : $scope.formData.TxtPhone,
    //            "office_lat" : $scope.formData.TxtLatitude,
                "client_id" : $scope.formData.SelClient,
                "id" : $scope.formData.DataID
            };
            console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();   
                    $scope.resetFormData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });     
        }else{
          alert("Password did not match");  
        }
        
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=user&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,            
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.callFormChangePass = function(id) {
        //alert(id);
        $scope.formData.formTitle = 'Change Password';          
        $scope.resetFormPass(id);  
        $scope.getFormData('modal-change-password');  
    };
    
    $scope.resetFormPass = function(id) {          
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;
        //$scope.formData.TxtNama = '';
//        $scope.formData.TxtPhone = '';
//        $scope.formData.TxtEmail = '';
        $scope.formData.DataPassID = id;
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtConfirm = '';
        //$scope.formData.SelClient = '';
        
    };
    
    $scope.changePass = function() {        
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=changePassOnly';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,            
                "newpass": $scope.formData.TxtPassword,                        
                "id" : $scope.formData.DataPassID
            };
            console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });
        }else{
            alert("Password did not match"); 
        }             
    };


}]);

app.controller('jukirController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNama = '';
    $scope.formData.TxtKontak = '';
    $scope.formData.TxtAlamat = '';
    $scope.formData.TxtEmail = '';
    $scope.formData.SelLocation = '';
//    $scope.formData.SelTingkat = '';
    $scope.formData.SelDevice = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=jukir&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataJukirList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataJukirList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=jukir&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {            
            if (response.data.status == 'OK') {
                $scope.formData.dataDevice = response.data.dataDevice;
                $scope.formData.dataLocation = response.data.dataLocation;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNama = '';
        $scope.formData.TxtKontak = '';
        $scope.formData.TxtAlamat = '';
        $scope.formData.TxtEmail = '';
        $scope.formData.TxtKodeJukir = '';
        $scope.formData.SelLocation = '';
    //    $scope.formData.SelTingkat = '';
        $scope.formData.SelDevice = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Jukir';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=jukir&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "jukir_name" : $scope.formData.TxtNama,
            "jukir_phone" : $scope.formData.TxtKontak,
            "jukir_address" : $scope.formData.TxtAlamat,
            "jukir_email" : $scope.formData.TxtEmail,
            "parking_location" : $scope.formData.SelLocation,
            "jukir_code" : $scope.formData.TxtKodeJukir,
            "device_uuid" : $scope.formData.SelDevice
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNama = data.jukir_name;
        $scope.formData.TxtKodeJukir = data.jukir_code;
        $scope.formData.TxtAlamat = data.jukir_address;
        $scope.formData.TxtKontak = data.jukir_phone;
        $scope.formData.TxtEmail = data.jukir_email;
        $scope.formData.SelDevice = data.id_device;
        $scope.formData.SelLocation = data.id_parking_location;
        //$scope.formData.SelClient = data.id_client;
        $scope.formData.DataID = data.id_jukir;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Jukir';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=jukir&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "jukir_name" : $scope.formData.TxtNama,
            "jukir_phone" : $scope.formData.TxtKontak,
            "jukir_address" : $scope.formData.TxtAlamat,
            "jukir_email" : $scope.formData.TxtEmail,
            "parking_location" : $scope.formData.SelLocation,
            "jukir_code" : $scope.formData.TxtKodeJukir,
            "device_uuid" : $scope.formData.SelDevice,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=jukir&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,            
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.callFormChangePass = function(id) {
        //alert(id);
        $scope.formData.formTitle = 'Change Password';          
        $scope.resetFormPass(id);  
        $scope.getFormData('modal-change-password');  
    };
    
    $scope.resetFormPass = function(id) {          
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;
        //$scope.formData.TxtNama = '';
//        $scope.formData.TxtPhone = '';
//        $scope.formData.TxtEmail = '';
        $scope.formData.DataPassID = id;
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtConfirm = '';
        //$scope.formData.SelClient = '';
        
    };
    
    $scope.changePass = function() {        
        if($scope.formData.TxtPassword==$scope.formData.TxtConfirm){
            var urlParams = 'q=api&m=user&a=changePassOnly';
            var dataParams = {
                "usr": $scope.appData.username,
                "tkn": $scope.appData.usertoken,            
                "newpass": $scope.formData.TxtPassword,                        
                "id" : $scope.formData.DataPassID
            };
            //console.log(dataParams);
            dataService.POSTRequest(urlParams, dataParams).then(function( response ){
                if(response.data.status == 'OK') {
                    $scope.getData();
                    $rootScope.messagePopUp('success', response.data.message);
                } else {
                    $rootScope.messagePopUp('error', response.data.message);
                }
            });
        }else{
            alert("Password did not match"); 
        }             
    };


}]);

app.controller('systemsettingController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    //$scope.formData.TxtDeviceType = '';
//    $scope.formData.TxtDeviceCode = '';
//    $scope.formData.TxtDeviceUuid = '';
//    $scope.formData.TxtPhoneParent = '';
    $scope.formData.SelClient = '';
    $scope.formData.SelLocation = '';
    $scope.formData.SelTrxtype = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=system-setting&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                console.log(response.data);
                $scope.dataSystemList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataSystemList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    
    $scope.getLokasi = function(idlokasi) {
        //alert($scope.formData.SelClient);return false;
        $scope.formData.SelLocation = '';
        var urlParams = 'q=api&m=system-setting&a=get-form-lokasi';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "client_id": $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
//                $('#tbl-data-table').DataTable().destroy();
//            }
            
            if(response.data.status == 'OK') {
                $scope.formData.dataLocation = response.data.dataLocation;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                }
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=system-setting&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            //console.log(response.data);
            if (response.data.status == 'OK') {
                $scope.formData.dataTrxtype = response.data.dataTrxtype;
                $scope.formData.dataLocation = response.data.dataLocation;
                $scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        //$scope.formData.TxtDeviceType = '';
//        $scope.formData.TxtDeviceCode = '';
//        $scope.formData.TxtDeviceUuid = '';
//        $scope.formData.TxtPhoneParent = '';
        $scope.formData.SelTrxtype = '';
        $scope.formData.SelLocation = '';
        $scope.formData.SelClient = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah System';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=system-setting&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "client_id" : $scope.formData.SelClient,
            "trx_type" : $scope.formData.SelTrxtype,
            "parking_location" : $scope.formData.SelLocation,
            //"jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            console.log(response.data);
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.SelClient = data.id_client;
        $scope.formData.SelLocation = data.id_parking_location;
        $scope.formData.SelTrxtype = data.id_trx_type;
        //$scope.formData.SelJukir = data.id_jukir;
        //$scope.formData.SelLocation = data.id_parking_location;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_system_setting;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data System';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=system-setting&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "client_id" : $scope.formData.SelClient,
            "trx_type" : $scope.formData.SelTrxtype,
            "parking_location" : $scope.formData.SelLocation,
            //"jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=system-setting&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('parkingslotController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    //$scope.formData.TxtDeviceType = '';
//    $scope.formData.TxtDeviceCode = '';
//    $scope.formData.TxtDeviceUuid = '';
//    $scope.formData.TxtPhoneParent = '';    
    $scope.formData.SelLocation = '';
    $scope.formData.SelVehicle = '';
    $scope.formData.TxtQty = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=parking-slot&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                console.log(response.data);
                $scope.dataParkSlotList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataParkSlotList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    
    $scope.getLokasi = function(idlokasi) {
        //alert($scope.formData.SelClient);return false;
        $scope.formData.SelLocation = '';
        var urlParams = 'q=api&m=parking-slot&a=get-form-lokasi';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "client_id": $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
//                $('#tbl-data-table').DataTable().destroy();
//            }
            
            if(response.data.status == 'OK') {
                $scope.formData.dataLocation = response.data.dataLocation;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                }
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=parking-slot&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            //console.log(response.data);
            if (response.data.status == 'OK') {
                $scope.formData.dataLocation = response.data.dataLocation;
                $scope.formData.dataVehicle = response.data.dataVehicle;                
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        //$scope.formData.TxtDeviceType = '';
//        $scope.formData.TxtDeviceCode = '';
//        $scope.formData.TxtDeviceUuid = '';
//        $scope.formData.TxtPhoneParent = '';
        $scope.formData.SelLocation = '';
        $scope.formData.SelVehicle = '';        
        $scope.formData.TxtQty = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Slot Parkir';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=parking-slot&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "parking_location" : $scope.formData.SelLocation,
            "vehicle_type" : $scope.formData.SelVehicle,
            "capacity" : $scope.formData.TxtQty,            
            //"jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            console.log(response.data);
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.SelLocation = data.id_parking_location;
        $scope.formData.SelVehicle = data.id_vehicle_type;        
        $scope.formData.TxtQty = data.capacity;
        //$scope.formData.SelJukir = data.id_jukir;
        //$scope.formData.SelLocation = data.id_parking_location;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_parking_slot;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Slot Parkir';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=parking-slot&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "parking_location" : $scope.formData.SelLocation,
            "vehicle_type" : $scope.formData.SelVehicle,
            "capacity" : $scope.formData.TxtQty,            
            //"jukir_id" : $scope.formData.SelJukir,
            //"parking_location" : $scope.formData.SelLocation,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        //console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        //alert(id);
        var urlParams = 'q=api&m=parking-slot&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('parkinglocationController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtLocationName = '';
    $scope.formData.TxtLocationCode = '';
    $scope.formData.TxtLongitude = '';
    $scope.formData.TxtLatitude = '';
//    $scope.formData.SelSex = '';
    $scope.formData.SelTarif = '';
//    $scope.formData.SelTingkat = '';
    //$scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=parking_location&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataLokasiList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataLokasiList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=parking_location&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.formData.dataTarifType = response.data.dataTarifType;
//                $scope.formData.dataTingkat = response.data.dataTingkat;
                //$scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtLocationName = '';
        $scope.formData.TxtLocationCode = '';
        $scope.formData.TxtLongitude = '';
        $scope.formData.TxtLatitude = '';
        $scope.formData.SelTarif = '';
//        $scope.formData.SelKampus = '';
//        $scope.formData.SelTingkat = '';
        //$scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Lokasi';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=parking_location&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"alamat" : $scope.formData.TxtAlamat,
            "parking_location_name" : $scope.formData.TxtLocationName,
            "parking_location_code" : $scope.formData.TxtLocationCode,
            "parking_location_long" : $scope.formData.TxtLongitude,
            "parking_location_lat" : $scope.formData.TxtLatitude,
            "trx_type" : $scope.formData.SelTarif,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtLocationName = data.parking_location_name;
        $scope.formData.TxtLocationCode = data.parking_location_code;
        $scope.formData.TxtLongitude = data.parking_location_long;
        $scope.formData.TxtLatitude = data.parking_location_lat;
        $scope.formData.SelTarif = data.id_trx_type;
//        $scope.formData.SelTingkat = data.tingkat_id;
        //$scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.id_parking_location;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Lokasi';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=parking_location&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "parking_location_name" : $scope.formData.TxtLocationName,
            "parking_location_code" : $scope.formData.TxtLocationCode,
            "parking_location_long" : $scope.formData.TxtLongitude,
            "parking_location_lat" : $scope.formData.TxtLatitude,
            "trx_type" : $scope.formData.SelTarif,
//            "tingkat_id" : $scope.formData.SelTingkat,
            //"client" : $scope.formData.SelClient,
            "id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=parking_location&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);
                
app.controller('laporanController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {

    $scope.appData = {};
    $scope.formData = {};   

    // $('#preloader').show();

    dataService.GETRequest('q=is-login').then(function (response) {
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.email;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function () {
                $scope.getData();
            }, 0);
        }
    });  
    
    $scope.getData = function () {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=responden&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if ($.fn.DataTable.isDataTable('#tbl-data-table')) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if (response.data.status == 'OK') {
                $scope.dataUser = response.data.dataRecords;
                $timeout(function () {
                    $("#tbl-data-table").DataTable({
                        "destroy": true,
                        "initComplete": function () {
                            //$('#preloader').hide('fast');
                        }
                    });
                }, 0);
            } else {
                $scope.dataUser = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function () {
                    if(response.data.message == 'Oops! Authentication failed.') {      
                        location.href = 'login.html';
                    }
                    $("#tbl-data-table").DataTable({
                        "destroy": true,
                        "initComplete": function () {
                            //$('#preloader').hide('fast');
                        }
                    });
                }, 0);
            }
        });
    };   

    $scope.getDetailData = function (profil_responden_id) {
        $scope.profileParams.isEditProfile = false;  
        var urlParams = 'q=api&m=responden&a=get-detail';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "profil_responden_id" : profil_responden_id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if ($.fn.DataTable.isDataTable('#tbl-data-table')) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if (response.data.status == 'OK') {
                $scope.dataUserProfile = response.data.dataUserProfile; 
                $scope.dataSosialEkonomi = response.data.dataSosialEkonomi; 
                $scope.dataKualitasHidup = response.data.dataKualitasHidup; 
                $scope.dataPrestasiProgramA = response.data.dataPrestasiProgramA; 
                $scope.dataPrestasiProgramB = response.data.dataPrestasiProgramB; 
                $scope.dataPrestasiProgramC = response.data.dataPrestasiProgramC; 
                $scope.profileParams.isEditProfile = true;
                $('#modal-detail-data').modal('toggle');   
            } else {
                $scope.dataUserProfile = {};
                $rootScope.messagePopUp('error', response.data.message);  
            }
        });
    };   

}]); 

app.controller('logoutController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {
    var urlParams = 'q=logout';
    dataService.GETRequest(urlParams).then(function (response) {
        alert(response);
    });
}]);

app.controller('changePasswordController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {

}]);