app.controller('mainController', ['$scope', '$timeout', '$interval', 'dataService', '$rootScope', function ($scope, $timeout, $interval, dataService, $rootScope) {
    $scope.appData = {};

    //$('#preloader').show('fast');

    dataService.GETRequest('q=is-login').then(function (response) {
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function () {              
                $('#main-content').show();
                //$scope.getData();
            }, 0);
        } else {
            location.href = 'login.html';
        }
    });

//    $scope.getData = function () {
//        var urlParams = 'q=api&m=dashboard&a=get';
//        var dataParams = {
//            "usr": $scope.appData.username,
//            "tkn": $scope.appData.usertoken
//        };
//        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
//            if (response.data.status == 'OK') {
//                $timeout(function () {
//                    $scope.appData.numRecordsKaryawan = response.data.numRecordsKaryawan;
//                }, 0);
//            } else {
//                $rootScope.messagePopUp('error', response.data.message);
//            }
//        });
//    };

}]);

app.controller('welcomeController', ['$scope', '$timeout', '$interval', 'dataService', '$rootScope', function ($scope, $timeout, $interval, dataService, $rootScope) {
    $scope.appData = {};
    $('#preloader').hide('fast');

    dataService.GETRequest('q=is-login').then(function (response) {
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username; 
            $scope.appData.kampus = response.data.dataAuth.kampus; 
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
        } else {
            location.href = 'login.html';
        }
    });

}]);

app.controller('siswaController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNamaSiswa = '';
    $scope.formData.TxtNIS = '';
    $scope.formData.TxtEmailParent = '';
    $scope.formData.TxtPhoneParent = '';
    $scope.formData.SelSex = '';
    $scope.formData.SelKampus = '';
    $scope.formData.SelTingkat = '';
    $scope.formData.SelHalaqah = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=siswa&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataSiswaList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataSiswaList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };

    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=siswa&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.formData.dataKampus = response.data.dataKampus;
                $scope.formData.dataTingkat = response.data.dataTingkat;
                $scope.formData.dataHalaqah = response.data.dataHalaqah;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNamaSiswa = '';
        $scope.formData.TxtNIS = '';
        $scope.formData.TxtEmailParent = '';
        $scope.formData.TxtPhoneParent = '';
        $scope.formData.SelSex = '';
        $scope.formData.SelKampus = '';
        $scope.formData.SelTingkat = '';
        $scope.formData.SelHalaqah = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Siswa';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=siswa&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "nis" : $scope.formData.TxtNIS,
            "nama_siswa" : $scope.formData.TxtNamaSiswa,
            "email_orangtua" : $scope.formData.TxtEmailParent,
            "phone_orangtua" : $scope.formData.TxtPhoneParent,
            "jenis_kelamin" : $scope.formData.SelSex,
            "kampus_id" : $scope.formData.SelKampus,
            "tingkat_id" : $scope.formData.SelTingkat,
            "halaqah_id" : $scope.formData.SelHalaqah
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        $scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNamaSiswa = data.nama_siswa;
        $scope.formData.TxtEmailParent = data.email_orangtua;
        $scope.formData.TxtPhoneParent = data.phone_orangtua;
        $scope.formData.SelSex = data.jenis_kelamin;
        $scope.formData.SelKampus = data.kampus_id;
        $scope.formData.SelTingkat = data.tingkat_id;
        $scope.formData.SelHalaqah = data.halaqah_id;
        $scope.formData.DataID = data.siswa_id;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Siswa';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=siswa&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            "nis" : $scope.formData.TxtNIS,
            "nama_siswa" : $scope.formData.TxtNamaSiswa,
            "email_orangtua" : $scope.formData.TxtEmailParent,
            "phone_orangtua" : $scope.formData.TxtPhoneParent,
            "jenis_kelamin" : $scope.formData.SelSex,
            "kampus_id" : $scope.formData.SelKampus,
            "tingkat_id" : $scope.formData.SelTingkat,
            "halaqah_id" : $scope.formData.SelHalaqah,
            "siswa_id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=siswa&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "siswa_id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('kampusController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNamaKampus = '';
    //$scope.formData.TxtNIS = '';
    //$scope.formData.TxtEmailParent = '';
//    $scope.formData.TxtPhoneParent = '';
//    $scope.formData.SelSex = '';
//    $scope.formData.SelKampus = '';
//    $scope.formData.SelTingkat = '';
    $scope.formData.SelClient = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=kampus&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataKampusList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataKampusList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };

    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=kampus&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                //$scope.formData.dataKampus = response.data.dataKampus;
//                $scope.formData.dataTingkat = response.data.dataTingkat;
                $scope.formData.dataClient = response.data.dataClient;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNamaKampus = '';
        //$scope.formData.TxtNIS = '';
//        $scope.formData.TxtEmailParent = '';
//        $scope.formData.TxtPhoneParent = '';
//        $scope.formData.SelSex = '';
//        $scope.formData.SelKampus = '';
//        $scope.formData.SelTingkat = '';
        $scope.formData.SelClient = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Kampus';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=kampus&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"nis" : $scope.formData.TxtNIS,
            "nama_kampus" : $scope.formData.TxtNamaKampus,
            "alamat" : $scope.formData.TxtAlamat,
//            "phone_orangtua" : $scope.formData.TxtPhoneParent,
//            "jenis_kelamin" : $scope.formData.SelSex,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            "client_id" : $scope.formData.SelClient
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNamaKampus = data.nama_kampus;
        $scope.formData.TxtAlamat = data.address;
//        $scope.formData.TxtPhoneParent = data.phone_orangtua;
//        $scope.formData.SelSex = data.jenis_kelamin;
//        $scope.formData.SelKampus = data.kampus_id;
//        $scope.formData.SelTingkat = data.tingkat_id;
        $scope.formData.SelClient = data.client_id;
        $scope.formData.DataID = data.kampus_id;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Kampus';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=kampus&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "nama_kampus" : $scope.formData.TxtNamaKampus,
            "alamat" : $scope.formData.TxtAlamat,
            //"phone_orangtua" : $scope.formData.TxtPhoneParent,
//            "jenis_kelamin" : $scope.formData.SelSex,
//            "kampus_id" : $scope.formData.SelKampus,
//            "tingkat_id" : $scope.formData.SelTingkat,
            "client_id" : $scope.formData.SelClient,
            "siswa_id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=kampus&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('muhafidzController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNamaMuhafidz = '';
    //$scope.formData.TxtNIS = '';
    $scope.formData.TxtEmail = '';
    $scope.formData.TxtPhone = '';
    //$scope.formData.SelSex = '';
    $scope.formData.SelKampus = '';
    //$scope.formData.SelTingkat = '';
//    $scope.formData.SelHalaqah = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=muhafidz&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //alert(JSON.stringify(response.data));return false;
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataMuhafidzList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataMuhafidzList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    //return false;
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=muhafidz&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.formData.dataKampus = response.data.dataKampus;
                //$scope.formData.dataTingkat = response.data.dataTingkat;
//                $scope.formData.dataHalaqah = response.data.dataHalaqah;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNamaMuhafidz = '';
        //$scope.formData.TxtNIS = '';
        $scope.formData.TxtEmail = '';
        $scope.formData.TxtPhone = '';
        //$scope.formData.SelSex = '';
        $scope.formData.SelKampus = '';
        //$scope.formData.SelTingkat = '';
//        $scope.formData.SelHalaqah = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Muhafidz';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=muhafidz&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"nis" : $scope.formData.TxtNIS,
            "nama" : $scope.formData.TxtNamaMuhafidz,
            "email" : $scope.formData.TxtEmail,
            "nohp" : $scope.formData.TxtPhone,
            //"jenis_kelamin" : $scope.formData.SelSex,
            "kampus_id" : $scope.formData.SelKampus
            //"tingkat_id" : $scope.formData.SelTingkat,
//            "halaqah_id" : $scope.formData.SelHalaqah
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNamaMuhafidz = data.nama;
        $scope.formData.TxtEmail = data.email;
        $scope.formData.TxtPhone = data.phone;
        //$scope.formData.SelSex = data.jenis_kelamin;
        $scope.formData.SelKampus = data.kampus_id;
        //$scope.formData.SelTingkat = data.tingkat_id;
//        $scope.formData.SelHalaqah = data.halaqah_id;
        $scope.formData.DataID = data.muhafidz_id;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Muhafidz';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=muhafidz&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "nama" : $scope.formData.TxtNamaMuhafidz,
            "email" : $scope.formData.TxtEmail,
            "nohp" : $scope.formData.TxtPhone,
            //"jenis_kelamin" : $scope.formData.SelSex,
            "kampus_id" : $scope.formData.SelKampus,
            //"tingkat_id" : $scope.formData.SelTingkat,
//            "halaqah_id" : $scope.formData.SelHalaqah,
            "id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=muhafidz&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('halaqahController', ['$scope', '$timeout','dataService', '$rootScope', function( $scope, $timeout, dataService, $rootScope ){

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.TxtNamaHalaqah = '';
    //$scope.formData.TxtNIS = '';
    //$scope.formData.TxtEmail = '';
//    $scope.formData.TxtPhone = '';
    //$scope.formData.SelSex = '';
    //$scope.formData.SelKampus = '';
    $scope.formData.SelTingkat = '';
//    $scope.formData.SelHalaqah = '';

    dataService.GETRequest('q=is-login').then(function( response ){
        if(response.data.status == 'OK') {                               
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function(){
                $scope.getData();
            }, 0);
        }
    }); 
    
    $scope.getData = function() {
        var urlParams = 'q=api&m=halaqah&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            //alert(JSON.stringify(response.data));return false;
            if ( $.fn.DataTable.isDataTable('#tbl-data-table') ) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if(response.data.status == 'OK') {
                $scope.dataHalaqahList = response.data.dataRecords;
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            } else {
                $scope.dataHalaqahList = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function(){
                    $("#tbl-data-table").DataTable({
                        "destroy" : true,
                        "initComplete": function() {
                            $('#preloader').hide('fast');
                        }
                      });
                }, 0);
            }
        });     
    };
    
    $scope.getFormData = function (modal_selector=null) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=halaqah&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                //$scope.formData.dataKampus = response.data.dataKampus;
                $scope.formData.dataTingkat = response.data.dataTingkat;
//                $scope.formData.dataHalaqah = response.data.dataHalaqah;
                if(modal_selector != null) {
                    $('#' + modal_selector).modal('show');
                    $('.select2').select2();
                } 
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };
    
    $scope.resetFormData = function() {   
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.TxtNamaHalaqah = '';
        //$scope.formData.TxtNIS = '';
        //$scope.formData.TxtEmail = '';
//        $scope.formData.TxtPhone = '';
        //$scope.formData.SelSex = '';
        //$scope.formData.SelKampus = '';
        $scope.formData.SelTingkat = '';
//        $scope.formData.SelHalaqah = '';
        $timeout(function(){    
            $('.select2').select2();
        }, 0);
    };

    $scope.callFormAddData = function() {
        $scope.formData.formTitle = 'Tambah Halaqah';          
        $scope.resetFormData();  
        $scope.getFormData('modal-form-data');  
    };
    
    $scope.createData = function() {
        var urlParams = 'q=api&m=halaqah&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            //"nis" : $scope.formData.TxtNIS,
            "nama_halaqah" : $scope.formData.TxtNamaHalaqah,
            //"email" : $scope.formData.TxtEmail,
//            "phone" : $scope.formData.TxtPhone,
            //"jenis_kelamin" : $scope.formData.SelSex,
            //"kampus_id" : $scope.formData.SelKampus
            "tingkat" : $scope.formData.SelTingkat,
//            "halaqah_id" : $scope.formData.SelHalaqah
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();        
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };  
    
    $scope.setFormEdit = function(data) { 
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;   
        //$scope.formData.TxtNIS = data.nis;
        $scope.formData.TxtNamaHalaqah = data.nama_halaqah;
        //$scope.formData.TxtEmail = data.email;
//        $scope.formData.TxtPhone = data.phone;
        //$scope.formData.SelSex = data.jenis_kelamin;
        //$scope.formData.SelKampus = data.kampus_id;
        $scope.formData.SelTingkat = data.tingkat_id;
//        $scope.formData.SelHalaqah = data.halaqah_id;
        $scope.formData.DataID = data.halaqah_id;
    };
    
    $scope.callFormEditData = function(data) {
        $scope.formData.formTitle = 'Edit Data Halaqah';
        $scope.setFormEdit(data);      
        $scope.getFormData('modal-form-data');
    };
    
    $scope.editData = function(id) {
        var urlParams = 'q=api&m=halaqah&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,   
            //"nis" : $scope.formData.TxtNIS,
            "nama_halaqah" : $scope.formData.TxtNamaHalaqah,
            //"email" : $scope.formData.TxtEmail,
//            "phone" : $scope.formData.TxtPhone,
            //"jenis_kelamin" : $scope.formData.SelSex,
            //"kampus_id" : $scope.formData.SelKampus,
            "tingkat" : $scope.formData.SelTingkat,
//            "halaqah_id" : $scope.formData.SelHalaqah,
            "id" : $scope.formData.DataID
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();   
                $scope.resetFormData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };
    
    $scope.deleteData = function(id) {
        var urlParams = 'q=api&m=halaqah&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "id" : id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function( response ){
            if(response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });     
    };


}]);

app.controller('userController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {

    $scope.appData = {};
    $scope.formData = {};
    $scope.formData.SelGroupPengguna = '';
    $scope.formData.SelUnit = '';
    $scope.formData.SelBagian = '';
    $scope.formData.SelPengguna = '';
    $scope.formData.TxtPassword = '';
    $scope.formData.TxtKonfirmasiPassword = '';

    // $('#preloader').show();

    dataService.GETRequest('q=is-login').then(function (response) {
        if (response.data.status == 'OK') {
            $scope.appData.username = response.data.dataAuth.username;
            $scope.appData.usertoken = response.data.dataAuth.usertoken;
            $timeout(function () {
                $scope.getData();
            }, 0);
        }
    });

    $scope.getData = function () {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=user&a=get';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if ($.fn.DataTable.isDataTable('#tbl-data-table')) {
                $('#tbl-data-table').DataTable().destroy();
            }
            if (response.data.status == 'OK') {
                $scope.dataUser = response.data.dataUser;
                $timeout(function () {
                    $("#tbl-data-table").DataTable({
                        "destroy": true,
                        "initComplete": function () {
                            //$('#preloader').hide('fast');
                        }
                    });
                }, 0);
            } else {
                $scope.dataUser = {};
                $rootScope.messagePopUp('error', response.data.message);
                $timeout(function () {
                    $("#tbl-data-table").DataTable({
                        "destroy": true,
                        "initComplete": function () {
                            //$('#preloader').hide('fast');
                        }
                    });
                }, 0);
            }
        });
    };

    $scope.getFormData = function () {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=user&a=get-form-data';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.formData.dataGroupUser = response.data.dataGroupUser;
                $scope.formData.dataUnit = response.data.dataUnit;
                $scope.formData.dataBagian = response.data.dataBagian;
                $scope.formData.dataKaryawan = response.data.dataKaryawan;
            } else {
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };

    $scope.callFormAddData = function () {
        $scope.getFormData();
        $scope.formData.formTitle = 'Tambah Pengguna';
        $scope.formData.showAddButton = true;
        $scope.formData.showEditButton = false;
        $scope.formData.SelGroupPengguna = '';
        $scope.formData.SelUnit = '';
        $scope.formData.SelBagian = '';
        $scope.formData.SelPengguna = '';
        $scope.formData.TxtPassword = '';
        $scope.formData.TxtKonfirmasiPassword = '';
    };

    $scope.createData = function () {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=user&a=post';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "user_tipe_id" : $scope.formData.SelGroupPengguna,
            "unit_id" : $scope.formData.SelUnit,
            "bagian_id" : $scope.formData.SelBagian,
            "nik" : $scope.formData.SelPengguna,
            "passwd" : $scope.formData.TxtPassword
        };
        console.log(dataParams);
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                //$('#preloader').hide('fast');
                $scope.getData();
                $scope.formData.SelGroupPengguna = '';
                $scope.formData.SelUnit = '';
                $scope.formData.SelBagian = '';
                $scope.formData.SelPengguna = '';
                $scope.formData.TxtPassword = '';
                $scope.formData.TxtKonfirmasiPassword = '';
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                //$('#preloader').hide('fast');table
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };

    $scope.callFormEditData = function (id) {
        $scope.formData.formTitle = 'Ubah';
        $scope.formData.showAddButton = false;
        $scope.formData.showEditButton = true;
        $scope.getDataById(id);
    };

    $scope.editData = function (id) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=unit&a=put';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $scope.getData();
                $rootScope.messagePopUp('success', response.data.message);
            } else {
                //$('#preloader').hide('fast');
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };

    $scope.deleteData = function (id) {
        //$('#preloader').show('fast');
        var urlParams = 'q=api&m=siswa&a=delete';
        var dataParams = {
            "usr": $scope.appData.username,
            "tkn": $scope.appData.usertoken,
            "user_id": id
        };
        dataService.POSTRequest(urlParams, dataParams).then(function (response) {
            if (response.data.status == 'OK') {
                $rootScope.messagePopUp('success', response.data.message);
                $scope.getData();
            } else {
                //$('#preloader').hide('fast');
                $rootScope.messagePopUp('error', response.data.message);
            }
        });
    };


}]);  

app.controller('logoutController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {
    var urlParams = 'q=logout';
    dataService.GETRequest(urlParams).then(function (response) {
        alert(response);
    });
}]);

app.controller('changePasswordController', ['$scope', '$timeout', 'dataService', '$rootScope', function ($scope, $timeout, dataService, $rootScope) {

}]);