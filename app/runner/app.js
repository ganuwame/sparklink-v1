
$(document).ready(function(){ 
    $.get(api_url + 'q=is-login', function(response){
        if(response.status == 'OK'){
            $('#main-content').show();    
        } else {           
            location.href = 'login.html';           
        } 
    });
});      

var app = angular.module("apptahfiz", ['ui.router', 'angular-loading-bar']);

app.run(function($rootScope) {
    $rootScope.messagePopUp = function(type, msg) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
        Toast.fire({
            type: type,
            title: '&nbsp;&nbsp;' + msg
        });
    };
});

app.directive('bindUnsafeHtml', ['$compile', function ($compile) {
  return function (scope, element, attrs) {
    scope.$watch(
      function (scope) {
        // watch the 'bindUnsafeHtml' expression for changes
        return scope.$eval(attrs.bindUnsafeHtml);
      },
      function (value) {
        // when the 'bindUnsafeHtml' expression changes
        // assign it into the current DOM
        element.html(value);

        // compile the new DOM and link it to the current
        // scope.
        // NOTE: we only compile .childNodes so that
        // we don't get into infinite loop compiling ourselves
        $compile(element.contents())(scope);
      }
    );
  };
}]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';
}]);