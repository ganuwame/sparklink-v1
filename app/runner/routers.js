app.config(['$stateProvider', function($stateProvider) {
    
  // An array of state definitions
  var states = [
  
    {
        name: 'home',
        url: '/',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'home/home.html',
                controller: 'mainController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },

    {
        name: 'otherwise',
        url: "*path",
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'home/home.html',
                controller: 'mainController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    }, 

    {
        name: 'vehicle',
        url: '/vehicle',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'vehicle/vehicle.html',
                controller: 'vehicleController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'tarif',
        url: '/tarif',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'tarif/tarif.html',
                controller: 'tarifController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'device',
        url: '/device',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'device/device.html',
                controller: 'deviceController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'client',
        url: '/client',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'client/client.html',
                controller: 'clientController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'admin',
        url: '/admin',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'admin/admin.html',
                controller: 'adminController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },  
    
    {
        name: 'jukir',
        url: '/jukir',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'jukir/jukir.html',
                controller: 'jukirController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    }, 
    
    {
        name: 'user',
        url: '/user',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'user/user.html',
                controller: 'userController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },   
    
    {
        name: 'system_setting',
        url: '/system_setting',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'system_setting/system_setting.html',
                controller: 'systemsettingController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'parking_slot',
        url: '/parking_slot',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'parking_slot/parking_slot.html',
                controller: 'parkingslotController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'parking_location',
        url: '/parking_location',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'parking_location/parking_location.html',
                controller: 'parkinglocationController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
    
    {
        name: 'datatransaksi',
        url: '/datatransaksi',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'datatransaksi/datatransaksi.html',
                controller: 'datatransaksiController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
        

    {
        name: 'laporan',
        url: '/laporan',
        views: {
            '@' : {
                templateUrl:  view_template_dir_path + 'laporan/laporan.html',
                controller: 'laporanController'
            },
            'welcome-user@' : {
                templateUrl:  view_template_dir_path + 'home/home.welcome.html',
                controller: 'welcomeController'
            },
            'notif@' : {
                templateUrl:  view_template_dir_path + 'home/home.notif.html',
                controller: 'welcomeController'
            }
        }
    },
        
    {
        name: 'logout',
        url: '/logout',
        views: {
            '@' : {
                controller: 'logoutController'
            }
        }
    }
  ];
  
  // Loop over the state definitions and register them
  states.forEach(function(state) {
    $stateProvider.state(state);
  });
}]);