
function login() {
    //alert("coba login..");
    var loginParams = {
        "usr" : encodeURIComponent($('#txt-user').val()),
        "pwd" : encodeURIComponent($('#txt-passwd').val())
    };
    $.ajax({
        type: "POST",
        url: api_url + 'q=get-token',
        data: JSON.stringify(loginParams),
        success: function(res) { 
            //console.log(res);
            if(res.status == 'OK') {
                location.href = 'index.html';
            } else {
                messagePopUp('error' ,res.message);
            }
        },        
        dataType: "json",
        contentType : "application/json"
    });
}

function logout() {
    $.ajax({
        type: "POST",
        url: api_url + 'q=logout',
        success: function(res) { 
            if(res.status == 'OK') {
                location.href = '';
            } else {
                alert(res.message);
            }
        },
        dataType: "json",
        contentType : "application/json"
    });
}

var messagePopUp = function(type, msg) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    Toast.fire({
        type: type,
        title: '&nbsp;&nbsp;' + msg
    });
};