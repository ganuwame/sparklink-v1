<?php

class mTarifProgressive extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                            
            !empty($data->normal_hour) &&
            !empty($data->progressive_denom) &&
            !empty($data->progressive_denom_max) &&
            !empty($data->progressive_hour_max) &&
            !empty($data->parking_location) &&
            !empty($data->client_id) 
        ){
            
            $qry = "INSERT INTO ". DBNAME .".tarif_progressive 
                    (normal_hour, progressive_denom, progressive_denom_max, progressive_hour_max, id_parking_location, id_client, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, ?, now(), ?)"; 
            //echo $qry;return false;
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiiiiii',//ssi        
                                   $data->normal_hour, 
                                   $data->progressive_denom, 
                                   $data->progressive_denom_max, 
                                   $data->progressive_hour_max, 
                                   $data->parking_location,
                                   $data->client_id,
                                   $_SESSION['user_id']
                                   );

            // sanitize                                                                                                            
            $data->normal_hour = addslashes((htmlspecialchars(strip_tags($data->normal_hour))));
            $data->progressive_denom = addslashes((htmlspecialchars(strip_tags($data->progressive_denom))));
            $data->progressive_denom_max = addslashes((htmlspecialchars(strip_tags($data->progressive_denom_max))));
            $data->progressive_hour_max = addslashes((htmlspecialchars(strip_tags($data->progressive_hour_max))));
            $data->parking_location = addslashes((htmlspecialchars(strip_tags($data->parking_location))));    
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created.")); //SAVE_SUCCESS   
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));//SAVE_FAIL
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => SAVE_PARAMS_INVALID));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                            
            !empty($data->normal_hour) &&
            !empty($data->progressive_denom) &&
            !empty($data->progressive_denom_max) &&
            !empty($data->progressive_hour_max) &&
            !empty($data->parking_location) &&
            !empty($data->client_id) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".tarif_progressive 
                    SET normal_hour= ?, progressive_denom= ?, progressive_denom_max= ?, progressive_hour_max= ?, id_parking_location= ?, id_client= ?, uby = ? , udt = now()
                    WHERE id_tarif_progressive = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiiiiiii',  
                                   $data->normal_hour, 
                                   $data->progressive_denom, 
                                   $data->progressive_denom_max, 
                                   $data->progressive_hour_max, 
                                   $data->parking_location,
                                   $data->client_id,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                                                 
            $data->normal_hour = addslashes((htmlspecialchars(strip_tags($data->normal_hour))));
            $data->progressive_denom = addslashes((htmlspecialchars(strip_tags($data->progressive_denom))));
            $data->progressive_denom_max = addslashes((htmlspecialchars(strip_tags($data->progressive_denom_max))));
            $data->progressive_hour_max = addslashes((htmlspecialchars(strip_tags($data->progressive_hour_max))));
            $data->parking_location = addslashes((htmlspecialchars(strip_tags($data->parking_location))));    
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));//EDIT_SUCCESS    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));//EDIT_FAIL
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => EDIT_PARAMS_INVALID));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".tarif_progressive 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_tarif_progressive = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_tarif_progressive = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".tarif_progressive WHERE isdeleted = 0" . $sql_str ;

        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryTingkat = "SELECT * FROM ". DBNAME .".tbl_tahfidz_tingkat WHERE isdeleted = 0";
        $qryHalaqah = "SELECT * FROM ". DBNAME .".tbl_tahfidz_halaqah WHERE isdeleted = 0";
        $qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataTingkat = $this->selectQueryAsArray($qryTingkat);
        $dataHalaqah = $this->selectQueryAsArray($qryHalaqah);
        $dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataTingkat" => $dataTingkat, "dataHalaqah" => $dataHalaqah, "dataKampus" => $dataKampus)); 
    }
    
}

$tarif_progressive = new mTarifProgressive();

switch( $tarif_progressive->a ) {
    
    case "post":
        $tarif_progressive->post();
    break;
    
    case "get":
        $tarif_progressive->get();
    break;
    
    case "get-form-data":
        $tarif_progressive->getFormData();
    break;   
    
    case "put":
        $tarif_progressive->put();
    break;
    
    case "delete":
        $tarif_progressive->delete();
    break;
}