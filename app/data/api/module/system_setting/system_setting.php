<?php

class mSystemSetting extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->client_id) &&
            !empty($data->trx_type) &&
            !empty($data->parking_location)                
        ){
            
            $qry = "INSERT INTO ". DBNAME.".system_setting 
                    (id_client,id_trx_type, id_parking_location, idt, iby) 
                    VALUES (?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiii',
                                   $data->client_id, 
                                   $data->trx_type,
                                   $data->parking_location,                                                                       
                                   $_SESSION['user_id']);

            // sanitize            
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));            
            $data->trx_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_type))));            
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->client_id) &&
            !empty($data->trx_type) &&
            !empty($data->parking_location) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".system_setting 
                    SET id_client= ?,id_trx_type= ?, id_parking_location= ?, uby = ? , udt = now()
                    WHERE id_system_setting = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiiii', 
                                   $data->client_id, 
                                   $data->trx_type,
                                   $data->parking_location,                                                                        
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));            
            $data->trx_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_type))));            
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".system_setting 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_system_setting = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND a.id_system_setting = ". $data->id : null;  
        $qry = "SELECT a.*,b.client_name,c.parking_location_name,d.trx_type FROM ".DBNAME.".system_setting a
        inner join ".DBNAME.".client b on a.id_client=b.id_client
        inner join ".DBNAME.".parking_location c on a.id_parking_location=c.id_parking_location
        inner join ".DBNAME.".trx_type d on a.id_trx_type=d.id_trx_type
        WHERE a.isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormLokasi() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->client_id) && $data->client_id != '') ? " AND id_client = ". $data->client_id : null;  
        //$qry = "SELECT * FROM ".DBNAME.".system_setting WHERE isdeleted = 0" . $sql_str ;
        $qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE isdeleted = 0". $sql_str;
        $dataLocation= $this->selectQueryAsArray($qryLocation);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataLocation" => $dataLocation)); 
        //if($this->sqlNumRows($qry) > 0) {
//            $numRecords = $this->sqlNumRows($qry);
//            $dataRecords = $this->selectQueryAsArray($qry);
//            http_response_code(200);
//            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
//                        
//        } else {                                     
//            http_response_code(200);
//            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
//        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryClient = "SELECT * FROM ". DBNAME .".client WHERE isdeleted = 0";
        $qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE isdeleted = 0";
        $qryTrxtype = "SELECT * FROM ". DBNAME .".trx_type WHERE isdeleted = 0";
        
        $dataClient= $this->selectQueryAsArray($qryClient);
        $dataLocation = $this->selectQueryAsArray($qryLocation);
        $dataTrxtype = $this->selectQueryAsArray($qryTrxtype);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataClient" => $dataClient, "dataLocation" => $dataLocation, "dataTrxtype" => $dataTrxtype)); 
    }
    
}

$systemsetting = new mSystemSetting();

switch( $systemsetting->a ) {
    
    case "post":
        $systemsetting->post();
    break;
    
    case "get":
        $systemsetting->get();
    break;   
    
    case "get-form-data":
        $systemsetting->getFormData();
    break;
    
    case "get-form-lokasi":
        $systemsetting->getFormLokasi();
    break;
    
    case "put":
        $systemsetting->put();
    break;
    
    case "delete":
        $systemsetting->delete();
    break;
}