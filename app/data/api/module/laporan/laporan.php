<?php

class mLaporan extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->siswa_id) &&
            !empty($data->zd_hal_start) &&
            !empty($data->zd_hal_end) &&
            !empty($data->file_record)
//            !empty($data->surat) &&
//            !empty($data->dari_ayat) &&
//            !empty($data->sampai_ayat) &&
//            !empty($data->total_hal) &&
//            !empty($data->quran_halaman) &&
//            !empty($data->murajaah) &&            
//            !empty($data->tahun) &&
//            !empty($data->bulan) &&
//            !empty($data->pekan) 
        ){
            //SELECT siswa_id FROM ". DBNAME .".tbl_tahfidz_laporan WHERE (siswa_id = '". $data->siswa_id ."') 
//            AND isdeleted = 0 and cast(idt as date)= cast(now() as date)
            if($this->sqlNumRows("SELECT a.siswa_id FROM ". DBNAME .".tbl_tahfidz_laporan a 
            inner join ". DBNAME .".tbl_tahfidz_halaqah_siswa_list b on a.siswa_id=b.siswa_id
            inner join ". DBNAME .".tbl_tahfidz_halaqah c on b.halaqah_id = c.halaqah_id
            WHERE a.siswa_id = '". $data->siswa_id ."' and c.tingkat_id<>2
            AND a.isdeleted = 0 and cast(a.idt as date)= cast(now() as date)
            ") == 0) {
                $qry = "INSERT INTO ". DBNAME .".tbl_tahfidz_laporan 
                        (siswa_id, ziyadah_hal_start, ziyadah_hal_end, ziyadah_hal_total,
                        ziyadah_surat_start, ziyadah_surat_end, 
                        ziyadah_ayat_start, ziyadah_ayat_end, ziyadah_baris_start,  
                        ziyadah_baris_end, ziyadah_baris_total,   
                        tahun,bulan,pekan,                                           
                        record_filepath, idt, iby) 
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?)";
                        //?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                //echo $_SESSION['user_id'];return false;
                //ziyadah_surat_start, ziyadah_surat_end, 
//                        ziyadah_ayat_start, ziyadah_ayat_end, ziyadah_baris_start,
//                        ziyadah_baris_end, ziyadah_baris_total,
//                        tahun,bulan,pekan,
                $stmt = mysqli_prepare($this->dc, $qry); //iiii
                mysqli_stmt_bind_param($stmt, 'iiiiiiiiiiiiiisi', 
                                       $data->siswa_id, 
                                       $data->zd_hal_start,
                                       $data->zd_hal_end,
                                       $data->zd_hal_total,                                       
                                       $data->zd_surat_start,
                                       $data->zd_surat_end,
                                       $data->zd_ayat_start,
                                       $data->zd_ayat_end,//                                       
                                       $data->zd_baris_start,                                                                          
                                       $data->zd_baris_end,
                                       $data->zd_baris_total,                                       
                                       $data->tahun,
                                       $data->bulan,
                                       $data->pekan,
                                       $data->file_record,            
                                       $_SESSION['user_id']
                                       ); //iiiiiisiiisi

                // sanitize            
                $data->siswa_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->siswa_id))));
                $data->zd_hal_start = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_hal_start))));
                $data->zd_hal_end = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_hal_end))));
                $data->zd_hal_total = ($data->zd_hal_end - $data->zd_hal_start) + 1 ;            
                $data->file_record = addslashes(htmlspecialchars(strip_tags($data->file_record)));
                
                $data->zd_surat_start = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_surat_start))));
                $data->zd_surat_end = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_surat_end))));
                $data->zd_ayat_start = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_ayat_start))));
                $data->zd_ayat_end = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_ayat_end))));                
                $data->zd_baris_start = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_baris_start))));
                $data->zd_baris_end = addslashes(strtoupper(htmlspecialchars(strip_tags($data->zd_baris_end))));
                $data->zd_baris_total = ($data->zd_baris_end - $data->zd_baris_start) + 1 ;                
                $data->tahun = addslashes(htmlspecialchars(strip_tags($data->tahun)));
                $data->bulan = addslashes(htmlspecialchars(strip_tags($data->bulan)));
                $data->pekan = addslashes(htmlspecialchars(strip_tags($data->pekan)));                

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was created."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
                }
            }
            else{
                http_response_code(200); // set response code - 400 bad request
                echo json_encode(array("status" => "REPORTED", "message" => "siswa_id Already Reported Today."));
            }  
                                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->siswa_id) &&
            !empty($data->surat) &&
            !empty($data->dari_ayat) &&
            !empty($data->sampai_ayat) &&
            !empty($data->dari_hal) &&
            !empty($data->sampai_hal) &&
            !empty($data->total_hal) &&
            !empty($data->quran_halaman) &&
            !empty($data->murajaah) &&
            !empty($data->file_record) &&
            !empty($data->tahun) &&
            !empty($data->bulan) &&
            !empty($data->pekan) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_laporan 
                    SET siswa_id= ?, surat= ?, ziyadah_dari_ayat= ?, ziyadah_sampai_ayat= ?,
                    ziyadah_dari_hal= ?, ziyadah_sampai_hal= ?,total_hal= ?, 
                    halaman_quran= ?, murajaah_hal_baris= ?, record_filepath= ?,
                    tahun= ?, bulan= ?, pekan= ?, keterangan= ?, 
                    uby = ? , udt = now()
                    WHERE laporan_id = ?";//tingkat_id= ?, halaqah_id= ?, id_muhafidz = ?,
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiiiiiiiisiiisii', 
                                   $data->siswa_id, 
                                   $data->surat, 
                                   $data->dari_ayat,
                                   $data->sampai_ayat,
                                   $data->dari_hal,
                                   $data->sampai_hal,
                                   $data->total_hal,
                                   $data->quran_halaman,
                                   $data->murajaah,            
                                   $data->file_record,
                                   $data->tahun,
                                   $data->bulan,
                                   $data->pekan,
                                   $data->keterangan,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->siswa_id= addslashes(strtoupper(htmlspecialchars(strip_tags($data->siswa_id))));
            $data->surat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->surat))));
            $data->dari_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_ayat))));
            $data->sampai_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_ayat))));
            $data->dari_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_hal))));
            $data->sampai_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_hal))));
            $data->total_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->total_hal))));
            $data->quran_halaman = addslashes(strtoupper(htmlspecialchars(strip_tags($data->quran_halaman))));
            $data->murajaah = addslashes(strtoupper(htmlspecialchars(strip_tags($data->murajaah))));
            $data->file_record = addslashes(strtoupper(htmlspecialchars(strip_tags($data->file_record))));
            $data->tahun = addslashes(strtoupper(htmlspecialchars(strip_tags($data->tahun))));
            $data->bulan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->bulan))));
            $data->pekan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->pekan))));
            $data->keterangan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->keterangan))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_laporan 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE laporan_id = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'si', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".tbl_tahfidz_laporan WHERE isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getSiswaLaporDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        $qry = "                               
                select a.siswa_id,b.nama_siswa,'1' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                and a.isdeleted = 0
                union all
                select g.siswa_id,g.nama_siswa,'0' setor from
                (
                select a.siswa_id,b.nama_siswa,'1' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                and a.isdeleted = 0
                ) f
                right join (
                select a.siswa_id,a.nama_siswa,'0' setor
                from tbl_tahfidz_siswa a 
                inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list b on a.siswa_id=b.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah c on b.halaqah_id=c.halaqah_id
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where d.id_muhafidz='".$_SESSION['user_id']."'
                ) g on f.siswa_id=g.siswa_id
                where f.siswa_id is null
        " ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getSumLaporDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        $qry = "
                select j.*,(j.setor+j.belum) total from 
                (
                SELECT i.nama_muhafidz_grup,i.nama_muhafidz,
                  GROUP_CONCAT((CASE i.setor WHEN 'setor' THEN i.ttl ELSE NULL END)) AS setor,
                  GROUP_CONCAT((CASE i.setor WHEN 'belum' THEN i.ttl ELSE NULL END)) AS belum  
                FROM (
                select count(*) ttl,h.nama_muhafidz_grup,h.nama_muhafidz,setor from
                (
                select a.siswa_id,b.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'setor' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                union all
                select g.siswa_id,g.nama_siswa,g.nama_muhafidz_grup,g.nama_muhafidz,'belum' setor from
                (
                select a.siswa_id,b.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'setor' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                ) f
                right join (
                select a.siswa_id,a.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'belum' setor
                from ".DBNAME.".tbl_tahfidz_siswa a 
                inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list b on a.siswa_id=b.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah c on b.halaqah_id=c.halaqah_id
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where d.id_muhafidz='".$_SESSION['user_id']."'
                ) g on f.siswa_id=g.siswa_id
                where f.siswa_id is null
                ) h
                group by h.nama_muhafidz_grup, h.nama_muhafidz,h.setor
                )i
                GROUP BY i.nama_muhafidz_grup,i.nama_muhafidz
                ) j                                               
        " ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getSumHalaqahDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        $qry = "                
                select count(*) siswa,sum(h.jum) record,h.nama_halaqah,h.muhafidz,h.setor from
                (
                select count(*) jum,f.siswa_id,f.nama_siswa,f.nama_halaqah,f.muhafidz,f.setor
                from (
                select a.siswa_id,b.nama_siswa,c.nama_halaqah,d.nama muhafidz,'setor' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                where cast(a.idt as date)=cast(DATE_SUB(NOW(), INTERVAL 6 DAY) as date) and a.iby='".$_SESSION['user_id']."'    
                ) f
                group by f.siswa_id,f.nama_siswa,f.nama_halaqah,f.muhafidz,f.setor
                union all    
                select count(*) jum,g.siswa_id,g.nama_siswa,g.nama_halaqah,g.muhafidz,'belum' setor from
                    (
                      select a.siswa_id,b.nama_siswa,c.nama_halaqah,d.nama muhafidz,'setor' setor
                      from ".DBNAME.".tbl_tahfidz_laporan a 
                      inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                      inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                      inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                      inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                      where cast(a.idt as date)=cast(DATE_SUB(NOW(), INTERVAL 6 DAY) as date) and a.iby='".$_SESSION['user_id']."'
                    ) f
                    right join (
                      select a.siswa_id,a.nama_siswa,c.nama_halaqah,d.nama muhafidz,d.muhafidz_id,'belum' setor
                      from ".DBNAME.".tbl_tahfidz_siswa a 
                      inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list b on a.siswa_id=b.siswa_id
                      inner join ".DBNAME.".tbl_tahfidz_halaqah c on b.halaqah_id=c.halaqah_id
                      inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                      inner join ".DBNAME.".tbl_user e on d.muhafidz_id = e.nis_idmuhafidz
                      where e.user_id='".$_SESSION['user_id']."'
                    ) g on f.siswa_id=g.siswa_id
                    where f.siswa_id is null    
                    group by g.siswa_id,g.nama_siswa,g.nama_halaqah,g.muhafidz
                  )h group by h.nama_halaqah, h.setor,h.muhafidz                                            
        " ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getDetHalaqahDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
        if(
            !empty($data->halaqah_id)
        //$sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        ){
            //SELECT siswa_id FROM ". DBNAME .".tbl_tahfidz_laporan WHERE (siswa_id = '". $data->siswa_id ."') 
//            AND isdeleted = 0 and cast(idt as date)= cast(now() as date)
            $qry = "                                                     
                  select h.* from (
                    select count(*) jum,f.siswa_id,f.nama_siswa,f.nama_halaqah,f.halaqah_id,f.muhafidz,f.setor
                    from (
                    select a.siswa_id,b.nama_siswa,c.nama_halaqah,c.halaqah_id,d.nama muhafidz,'setor' setor
                    from ".DBNAME.".tbl_tahfidz_laporan a 
                    inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                    inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                    inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                    inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                    where cast(a.idt as date)=cast(DATE_SUB(NOW(), INTERVAL 7 DAY) as date) and a.iby='".$_SESSION['user_id']."'    
                    ) f
                    group by f.siswa_id,f.nama_siswa,f.nama_halaqah,f.halaqah_id,f.muhafidz,f.setor
                    union all    
                    select count(*) jum,g.siswa_id,g.nama_siswa,g.nama_halaqah,g.halaqah_id,g.muhafidz,'belum' setor from
                        (
                          select a.siswa_id,b.nama_siswa,c.nama_halaqah,c.halaqah_id,d.nama muhafidz,'setor' setor
                          from ".DBNAME.".tbl_tahfidz_laporan a 
                          inner join ".DBNAME.".tbl_tahfidz_siswa b on a.siswa_id=b.siswa_id
                          inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list e on a.siswa_id=e.siswa_id
                          inner join ".DBNAME.".tbl_tahfidz_halaqah c on e.halaqah_id=c.halaqah_id
                          inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                          where cast(a.idt as date)=cast(DATE_SUB(NOW(), INTERVAL 7 DAY) as date) and a.iby='".$_SESSION['user_id']."'
                        ) f
                        right join (
                          select a.siswa_id,a.nama_siswa,c.nama_halaqah,c.halaqah_id,d.nama muhafidz,d.muhafidz_id,'belum' setor
                          from tbl_tahfidz_siswa a 
                          inner join ".DBNAME.".tbl_tahfidz_halaqah_siswa_list b on a.siswa_id=b.siswa_id
                          inner join ".DBNAME.".tbl_tahfidz_halaqah c on b.halaqah_id=c.halaqah_id
                          inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.muhafidz_id=d.muhafidz_id
                          inner join ".DBNAME.".tbl_user e on d.muhafidz_id = e.nis_idmuhafidz
                          where e.user_id='".$_SESSION['user_id']."'
                        ) g on f.siswa_id=g.siswa_id
                        where f.siswa_id is null     
                        group by g.siswa_id,g.nama_siswa,g.nama_halaqah,g.halaqah_id,g.muhafidz
                    ) h 
                    where h.halaqah_id='".$data->halaqah_id."'
                                                               
            " ;
            if($this->sqlNumRows($qry) > 0) {
                $numRecords = $this->sqlNumRows($qry);
                $dataRecords = $this->selectQueryAsArray($qry);
                http_response_code(200);
                $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                            
            } else {                                     
                http_response_code(200);
                echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
            }
                                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Halaqah shoul be not empty."));
        }        
    }
    
    function genWeekView() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND laporan_id = ". $data->id : null;  
        $qry = "select tgl_start,tgl_end,minggu from ".DBNAME.".tbl_tahfidz_weekref 
where date_format(tgl_start,'%m')<>date_format(tgl_end,'%m') limit 5";
        if($this->sqlNumRows($qry) > 0) {
//            
            while($rw=$this->selectQueryAsObject($qry)){
                echo $rw->tgl_start." <=> ".$rw->tgl_end;
            }
            //$numRecords = $this->sqlNumRows($qry);
//            $dataRecords = $this->selectQueryAsArray($qry);
//            http_response_code(200);
//            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
       
    
    function deleteFile() { 
        $tobedeleted = "uploads/".$_POST['filename'];
        //$tobedeleted = "view/assets/img/uploads/".$_POST['filename'];
        unlink("$tobedeleted");
    }
    
}

$laporan = new mLaporan();

switch( $laporan->a ) {
    
    case "post":
        $laporan->post();
    break;
    
    case "get":
        $laporan->get();
    break;   
    
    case "get-lapor-daily":
        $laporan->getSiswaLaporDaily();
    break; 
    
    case "get-sumlapor-daily":
        $laporan->getSumLaporDaily();
    break;   
    
    case "get-sumhalaqah-daily":
        $laporan->getSumHalaqahDaily();
    break;        
    
    case "get-dethalaqah-daily":
        $laporan->getDetHalaqahDaily();
    break;    
    
    case "put":
        $laporan->put();
    break;
    
    case "delete":
        $laporan->delete();
    break;
    
    case "gen-week-view":
        $laporan->genWeekView();
    break;   
    
    case "upload": 
        $laporan->uploadFile();
    break;
    
    case "delfile": 
        $laporan->deleteFile();
    break;
}