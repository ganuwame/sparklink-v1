<?php

class mLaporan extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->nis) &&
//            !empty($data->surat) &&
//            !empty($data->dari_ayat) &&
//            !empty($data->sampai_ayat) &&
            !empty($data->dari_hal) &&
            !empty($data->sampai_hal) &&
//            !empty($data->total_hal) &&
//            !empty($data->quran_halaman) &&
            !empty($data->murajaah)
//            !empty($data->file_record) &&
//            !empty($data->tahun) &&
//            !empty($data->bulan) &&
//            !empty($data->pekan) 
        ){
            
            $qry = "INSERT INTO ". DBNAME .".tbl_tahfidz_laporan 
                    (nis, surat, ziyadah_dari_ayat, ziyadah_sampai_ayat,ziyadah_dari_hal, ziyadah_sampai_hal,total_hal, 
                    halaman_quran, murajaah_hal_baris, record_filepath,  
                    tahun, bulan, pekan, keterangan, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?)";//, ?, ?, ?, ?, ? id_grup, id_grup_muhafidz, id_muhafidz, //  ?, ?, ?, ?, ?, ?, ?, ?,
            //echo $_SESSION['user_id'];return false;
            $stmt = mysqli_prepare($this->dc, $qry); //iiiiiisiiisi  //iiisiiis
            mysqli_stmt_bind_param($stmt, 'iiiiiiiiisiiisi', 
                                   $data->nis, 
                                   $data->surat, 
                                   $data->dari_ayat,
                                   $data->sampai_ayat,
                                   $data->dari_hal,
                                   $data->sampai_hal,
                                   $data->total_hal,                                   
                                   $data->quran_halaman,
                                   $data->murajaah,            
                                   $data->file_record,
                                   $data->tahun,
                                   $data->bulan,
                                   $data->pekan,
                                   $data->keterangan,
                                   $_SESSION['user_id']
                                   ); //iiiiiisiiisi

            // sanitize            
            $data->nis = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nis))));
            $data->surat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->surat))));
            $data->dari_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_ayat))));
            $data->sampai_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_ayat))));
            $data->dari_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_hal))));
            $data->sampai_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_hal))));
            $data->total_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->total_hal))));            
            $data->quran_halaman = addslashes(strtoupper(htmlspecialchars(strip_tags($data->quran_halaman))));
            $data->murajaah = addslashes(strtoupper(htmlspecialchars(strip_tags($data->murajaah))));
            $data->file_record = addslashes(strtoupper(htmlspecialchars(strip_tags($data->file_record))));
            $data->tahun = addslashes(strtoupper(htmlspecialchars(strip_tags($data->tahun))));
            $data->bulan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->bulan))));
            $data->pekan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->pekan))));
            $data->keterangan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->keterangan))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->nis) &&
            !empty($data->surat) &&
            !empty($data->dari_ayat) &&
            !empty($data->sampai_ayat) &&
            !empty($data->dari_hal) &&
            !empty($data->sampai_hal) &&
            !empty($data->total_hal) &&
            !empty($data->quran_halaman) &&
            !empty($data->murajaah) &&
            !empty($data->file_record) &&
            !empty($data->tahun) &&
            !empty($data->bulan) &&
            !empty($data->pekan) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_laporan 
                    SET nis= ?, surat= ?, ziyadah_dari_ayat= ?, ziyadah_sampai_ayat= ?,
                    ziyadah_dari_hal= ?, ziyadah_sampai_hal= ?,total_hal= ?, 
                    halaman_quran= ?, murajaah_hal_baris= ?, record_filepath= ?,
                    tahun= ?, bulan= ?, pekan= ?, keterangan= ?, 
                    uby = ? , udt = now()
                    WHERE id_laporan = ?";//id_grup= ?, id_grup_muhafidz= ?, id_muhafidz = ?,
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiiiiiiiisiiisii', 
                                   $data->nis, 
                                   $data->surat, 
                                   $data->dari_ayat,
                                   $data->sampai_ayat,
                                   $data->dari_hal,
                                   $data->sampai_hal,
                                   $data->total_hal,
                                   $data->quran_halaman,
                                   $data->murajaah,            
                                   $data->file_record,
                                   $data->tahun,
                                   $data->bulan,
                                   $data->pekan,
                                   $data->keterangan,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->nis= addslashes(strtoupper(htmlspecialchars(strip_tags($data->nis))));
            $data->surat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->surat))));
            $data->dari_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_ayat))));
            $data->sampai_ayat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_ayat))));
            $data->dari_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->dari_hal))));
            $data->sampai_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->sampai_hal))));
            $data->total_hal = addslashes(strtoupper(htmlspecialchars(strip_tags($data->total_hal))));
            $data->quran_halaman = addslashes(strtoupper(htmlspecialchars(strip_tags($data->quran_halaman))));
            $data->murajaah = addslashes(strtoupper(htmlspecialchars(strip_tags($data->murajaah))));
            $data->file_record = addslashes(strtoupper(htmlspecialchars(strip_tags($data->file_record))));
            $data->tahun = addslashes(strtoupper(htmlspecialchars(strip_tags($data->tahun))));
            $data->bulan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->bulan))));
            $data->pekan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->pekan))));
            $data->keterangan = addslashes(strtoupper(htmlspecialchars(strip_tags($data->keterangan))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_laporan 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_laporan = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'si', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_laporan = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".tbl_tahfidz_laporan WHERE isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getSiswaLaporDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND id_laporan = ". $data->id : null;  
        $qry = "                               
                select a.nis,b.nama_siswa,'1' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.nis=b.nis
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                and a.isdeleted = 0
                union all
                select g.nis,g.nama_siswa,'0' setor from
                (
                select a.nis,b.nama_siswa,'1' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.nis=b.nis
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                and a.isdeleted = 0
                ) f
                right join (
                select a.nis,a.nama_siswa,'0' setor
                from tbl_tahfidz_siswa a 
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup_list b on a.nis=b.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup c on b.id_grup_muhafidz=c.id_grup_muhafidz
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where d.id_muhafidz='".$_SESSION['user_id']."'
                ) g on f.nis=g.nis
                where f.nis is null
        " ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getSumLaporDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND id_laporan = ". $data->id : null;  
        $qry = "
                select j.*,(j.setor+j.belum) total from 
                (
                SELECT i.nama_muhafidz_grup,i.nama_muhafidz,
                  GROUP_CONCAT((CASE i.setor WHEN 'setor' THEN i.ttl ELSE NULL END)) AS setor,
                  GROUP_CONCAT((CASE i.setor WHEN 'belum' THEN i.ttl ELSE NULL END)) AS belum  
                FROM (
                select count(*) ttl,h.nama_muhafidz_grup,h.nama_muhafidz,setor from
                (
                select a.nis,b.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'setor' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.nis=b.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup_list e on a.nis=e.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup c on e.id_grup_muhafidz=c.id_grup_muhafidz
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                union all
                select g.nis,g.nama_siswa,g.nama_muhafidz_grup,g.nama_muhafidz,'belum' setor from
                (
                select a.nis,b.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'setor' setor
                from ".DBNAME.".tbl_tahfidz_laporan a 
                inner join ".DBNAME.".tbl_tahfidz_siswa b on a.nis=b.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup_list e on a.nis=e.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup c on e.id_grup_muhafidz=c.id_grup_muhafidz
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where cast(a.idt as date)=cast(now() as date) and a.iby='".$_SESSION['user_id']."'
                ) f
                right join (
                select a.nis,a.nama_siswa,c.nama_muhafidz_grup,d.nama_muhafidz,'belum' setor
                from ".DBNAME.".tbl_tahfidz_siswa a 
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup_list b on a.nis=b.nis
                inner join ".DBNAME.".tbl_tahfidz_muhafidz_grup c on b.id_grup_muhafidz=c.id_grup_muhafidz
                inner join ".DBNAME.".tbl_tahfidz_muhafidz d on c.id_muhafidz=d.id_muhafidz
                where d.id_muhafidz='".$_SESSION['user_id']."'
                ) g on f.nis=g.nis
                where f.nis is null
                ) h
                group by h.nama_muhafidz_grup, h.nama_muhafidz,h.setor
                )i
                GROUP BY i.nama_muhafidz_grup,i.nama_muhafidz
                ) j                                               
        " ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function genWeekView() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        //$sql_str = (isset($data->id) && $data->id != '') ? " AND id_laporan = ". $data->id : null;  
        $qry = "select tgl_start,tgl_end,minggu from ".DBNAME.".tbl_tahfidz_weekref 
where date_format(tgl_start,'%m')<>date_format(tgl_end,'%m') limit 5";
        if($this->sqlNumRows($qry) > 0) {
//            
            while($rw=$this->selectQueryAsObject($qry)){
                echo $rw->tgl_start." <=> ".$rw->tgl_end;
            }
            //$numRecords = $this->sqlNumRows($qry);
//            $dataRecords = $this->selectQueryAsArray($qry);
//            http_response_code(200);
//            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function uploadFile() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        //$data = json_decode(file_get_contents("php://input"));
        if(
            //!empty($data->nis) &&
//            !empty($data->surat) &&
//            !empty($data->dari_ayat) &&
//            !empty($data->sampai_ayat) &&
//            !empty($data->quran_halaman) &&
//            !empty($data->murajaah) &&
//            !empty($data->file_record) &&
//            !empty($data->tahun) &&
//            !empty($data->bulan) &&
            !empty($_FILES) 
        ){
           $ds = DIRECTORY_SEPARATOR;
            $storeFolder = 'uploads';
             
            if (!empty($_FILES)) {
                $tempFile = $_FILES['file']['tmp_name'];           
                $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  
                $targetFile =  $targetPath. $_FILES['file']['name'];  
                move_uploaded_file($tempFile,$targetFile); 
            }  
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
                  
    }
    
    function deleteFile() { 
        $tobedeleted = "uploads/".$_POST['filename'];
        //$tobedeleted = "view/assets/img/uploads/".$_POST['filename'];
        unlink("$tobedeleted");
    }
    
}

$laporan = new mLaporan();

switch( $laporan->a ) {
    
    case "post":
        $laporan->post();
    break;
    
    case "get":
        $laporan->get();
    break;   
    
    case "get-lapor-daily":
        $laporan->getSiswaLaporDaily();
    break; 
    
    case "get-sumlapor-daily":
        $laporan->getSumLaporDaily();
    break;   
    
    case "put":
        $laporan->put();
    break;
    
    case "delete":
        $laporan->delete();
    break;
    
    case "gen-week-view":
        $laporan->genWeekView();
    break;   
    
    case "upload": 
        $laporan->uploadFile();
    break;
    
    case "delfile": 
        $laporan->deleteFile();
    break;
}