<?php

class mUser extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->name) &&
            !empty($data->username) &&
            !empty($data->email) &&
            !empty($data->phone) &&            
            !empty($data->client_id) &&            
            !empty($data->pass)
        ){
                        
            if($this->sqlNumRows("SELECT id_user FROM ". DBNAME .".tbl_user WHERE (email = '". $data->email ."') 
            AND isdeleted = 0") == 0) {
            
                $qry = "INSERT INTO ". DBNAME .".tbl_user 
                        (id_client,name,username,email,
                        phone,userlevel,usergroup, passwd, uid, idt, iby) 
                        VALUES (?,?,?,?,?,'1','1', md5(?), '0', now(), '".$_SESSION['user_id']."')";
                //echo $qry;
                //$token = md5($data->nis.date('Ymdhis'));        
                
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'isssis',                                        
                                       $data->client_id,
                                       $data->name,
                                       $data->username,
                                       $data->email,
                                       $data->phone,                                       
                                       $data->pass);

                // sanitize
                $data->client_id = strtoupper(htmlspecialchars(strip_tags($data->client_id)));                
                $data->name = strtoupper(htmlspecialchars(strip_tags($data->name)));                
                $data->username = strtoupper(htmlspecialchars(strip_tags($data->username)));                
                $data->email = strtoupper(htmlspecialchars(strip_tags($data->email)));
                $data->phone = strtoupper(htmlspecialchars(strip_tags($data->phone)));
                $data->pass = strtoupper(htmlspecialchars(strip_tags($data->pass)));                
                //$token = htmlspecialchars(strip_tags($token));  

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    $usr_id = mysqli_insert_id($this->dc);
                    $dataRegistration = $this->selectQueryAsObject("SELECT name, email, phone, token FROM ". DBNAME .".tbl_user 
                    WHERE id_user = '".$usr_id."' AND isdeleted = 0");
                    
                    echo json_encode(array("status" => OK, "dataRegistration" => $dataRegistration, "message" => "Record was created."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
                }     
            } else { 
                http_response_code(200); // set response code - 400 bad request
                echo json_encode(array("status" => "EXIST", "message" => "Account already exist."));
            }                  
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->name) &&            
            !empty($data->username) &&            
            !empty($data->email) &&
            !empty($data->phone) &&
            !empty($data->pass) &&
            !empty($data->client_id) &&            
            !empty($data->id)
        ){
            //if($this->stokIsNotUsed($data->master_sediaan_bahan_id)) {
                $qry = "UPDATE ". DBNAME .".tbl_user 
                        SET name= ?,username= ?,email= ?,
                        phone= ?,id_client= ?, passwd= ?,
                        uby = ? , udt = now()
                        WHERE id_user = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'ssssisii',                                        
                                       $data->name,
                                       $data->username,                                       
                                       $data->email,
                                       $data->phone,
                                       $data->client_id, 
                                       $data->pass, 
                                       $_SESSION['user_id'],
                                       $data->id);

                // sanitize                
                $data->name = strtoupper(htmlspecialchars(strip_tags($data->name)));
                $data->username = strtoupper(htmlspecialchars(strip_tags($data->username)));                
                $data->email = strtoupper(htmlspecialchars(strip_tags($data->email)));
                $data->phone = strtoupper(htmlspecialchars(strip_tags($data->phone)));
                $data->client_id = strtoupper(htmlspecialchars(strip_tags($data->client_id)));
                $data->pass = htmlspecialchars(strip_tags($data->pass));
                $data->id=htmlspecialchars(strip_tags($data->id));

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was updated."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
                }   
            //} else {    
//                http_response_code(200); // set response code - 400 bad request
//                echo json_encode(array("status" => NOT_OK, "message" => "Tidak dapat mengubah data. Stok sudah digunakan."));
//            }                    
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function putPass() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->oldpass) &&             
            !empty($data->newpass) 
        ){
            if($this->sqlNumRows("SELECT id_user FROM ". DBNAME .".tbl_user WHERE id_user='".$_SESSION['user_id']."' and passwd = md5('".$data->oldpass."') 
            AND isdeleted = 0") == 1) {
                $qry = "UPDATE ". DBNAME .".tbl_user 
                        SET passwd= md5(?),
                        uby = ? , udt = now()
                        WHERE id_user = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'sii', 
                                       $data->newpass,                                       
                                       $_SESSION['user_id'],
                                       $_SESSION['user_id']);

                // sanitize
                $data->newpass = addslashes(htmlspecialchars(strip_tags($data->newpass)));                

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was updated."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
                }   
            } else {    
                http_response_code(200); // set response code - 400 bad request
                echo json_encode(array("status" => NOT_OK, "message" => "Wrong Password or Anda bukan pemilik Account."));
            }                    
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function putPassOnly() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->newpass) && 
            !empty($data->id) 
        ){
            //if($this->sqlNumRows("SELECT id_user FROM ". DBNAME .".tbl_user WHERE id_user='".$_SESSION['user_id']."' and passwd = md5('".$data->oldpass."') 
//            AND isdeleted = 0") == 1) {
                $qry = "UPDATE ". DBNAME .".tbl_user 
                        SET passwd= md5(?),
                        uby = ? , udt = now()
                        WHERE id_user = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'sii', 
                                       $data->newpass,                                       
                                       $_SESSION['user_id'],
                                       $data->id);

                // sanitize
                //$data->newpass = addslashes($data->newpass);                
                //$data->newpass = addslashes(htmlspecialchars(strip_tags($data->newpass)));                

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was updated."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
                }   
            //} else {    
//                http_response_code(200); // set response code - 400 bad request
//                echo json_encode(array("status" => NOT_OK, "message" => "Wrong Password or Anda bukan pemilik Account."));
//            }                    
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
            //if($this->stokIsNotUsed($data->master_sediaan_bahan_id)) {
                
                $qry = "UPDATE ". DBNAME .".tbl_user 
                        SET isdeleted = 1, dby = ? , ddt = now()
                        WHERE id_user = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'ii', 
                                       $_SESSION['user_id'], 
                                       $data->id);

                // sanitize    
                $data->id=htmlspecialchars(strip_tags($data->id));

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
                }      
            //} else {    
//                http_response_code(200); // set response code - 400 bad request
//                echo json_encode(array("status" => NOT_OK, "message" => "Tidak dapat menghapus data. Stok sudah digunakan."));
//            }                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
        
    function stokIsNotUsed($user_sediaan_bahan_id) {
        $qry = "select master_sediaan_bahan_id FROM tbl_stok_penerimaan WHERE master_sediaan_bahan_id = ". $user_sediaan_bahan_id ." AND isdeleted = 0";
        if($this->sqlNumRows($qry) > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
        //$sql_str2 = ($_SESSION['user_group']>1) ? " AND m.iby = ".$_SESSION['user_id'] : null;
        $sql_str = (isset($data->id) && $data->id != '') ? " AND u.id_user = ". $data->id : null;  
        $qry = "select u.* from 
                (
                select tu.*,tu.name uname,concat('ADMAPP',id_user) ucode from ".DBNAME.".tbl_user tu where usergroup =1
                union all
                select tu.*,c.client_name as uname,c.client_code as ucode from ".DBNAME.".tbl_user tu 
                inner join ".DBNAME.".client c on c.id_client=tu.id_client
                where tu.username like 'client%' and  tu.isdeleted =0
                union all
                select tu.*,c.admin_name as uname,c.admin_code as ucode from ".DBNAME.".tbl_user tu 
                inner join ".DBNAME.".client_admin c on c.id_client_admin =tu.uid
                where tu.username like 'admin%' and tu.isdeleted =0
                union all
                select tu.*,c.jukir_name as uname,c.jukir_code as ucode from ".DBNAME.".tbl_user tu 
                inner join ".DBNAME.".jukir c on c.id_jukir =tu.uid 
                where tu.username like 'jukir%' and tu.isdeleted =0
                ) u                
                where u.isdeleted=0 " . $sql_str . " order by u.id_user asc";
        //echo $qry;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryClient = "SELECT * FROM ". DBNAME .".client WHERE isdeleted = 0";
        //$qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE isdeleted = 0";
//        $qryTrxtype = "SELECT * FROM ". DBNAME .".trx_type WHERE isdeleted = 0";
        
        $dataClient= $this->selectQueryAsArray($qryClient);
        //$dataLocation = $this->selectQueryAsArray($qryLocation);
//        $dataTrxtype = $this->selectQueryAsArray($qryTrxtype);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataClient" => $dataClient)); 
        //, "dataLocation" => $dataLocation, "dataTrxtype" => $dataTrxtype
    }
    
}

$user = new mUser();

switch( $user->a ) {
    
    case "post":
        $user->post();
    break;
    
    case "get":
        $user->get();
    break;   
    
    case "get-form-data":
        $user->getFormData();
    break;
    
    case "put":
        $user->put();
    break;
    
    case "changePass":
        $user->putPass();
    break;
    
    case "changePassOnly":
        $user->putPassOnly();
    break;
    
    case "delete":
        $user->delete();
    break;
}