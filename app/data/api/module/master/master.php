<?php

class mMaster extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->kategori) &&
            !empty($data->nama_sediaan_bahan) &&
            !empty($data->satuan)
        ){
            
            $qry = "INSERT INTO ". DBNAME .".tbl_master_sediaan_bahan 
                    (kategori, nama_sediaan_bahan, satuan, puskesmas_id, idt, iby) 
                    VALUES (?, ?, ?, ?, now(), ?)";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'issss', 
                                   $data->kategori, 
                                   $data->nama_sediaan_bahan, 
                                   $data->satuan, 
                                   $_SESSION['puskesmas_id'],
                                   $_SESSION['user_id']);

            // sanitize
            $data->kategori= htmlspecialchars(strip_tags($data->kategori));
            $data->nama_sediaan_bahan= strtoupper(htmlspecialchars(strip_tags($data->nama_sediaan_bahan)));
            $data->satuan= strtoupper(htmlspecialchars(strip_tags($data->satuan)));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->kategori) &&
            !empty($data->nama_sediaan_bahan) &&
            !empty($data->satuan) &&
            !empty($data->master_sediaan_bahan_id)
        ){
            if($this->stokIsNotUsed($data->master_sediaan_bahan_id)) {
                $qry = "UPDATE ". DBNAME .".tbl_master_sediaan_bahan 
                        SET kategori = ?, nama_sediaan_bahan = ?, satuan = ?, uby = ? , udt = now()
                        WHERE master_sediaan_bahan_id = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'isssi', 
                                       $data->kategori, 
                                       $data->nama_sediaan_bahan, 
                                       $data->satuan, 
                                       $_SESSION['user_id'],
                                       $data->master_sediaan_bahan_id);

                // sanitize
                $data->kategori=htmlspecialchars(strip_tags($data->kategori));
                $data->nama_sediaan_bahan= strtoupper(htmlspecialchars(strip_tags($data->nama_sediaan_bahan)));
                $data->satuan= strtoupper(htmlspecialchars(strip_tags($data->satuan)));
                $data->master_sediaan_bahan_id=htmlspecialchars(strip_tags($data->master_sediaan_bahan_id));

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was updated."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
                }   
            } else {    
                http_response_code(200); // set response code - 400 bad request
                echo json_encode(array("status" => NOT_OK, "message" => "Tidak dapat mengubah data. Stok sudah digunakan."));
            }                    
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->master_sediaan_bahan_id) 
        ){
            if($this->stokIsNotUsed($data->master_sediaan_bahan_id)) {
                
                $qry = "UPDATE ". DBNAME .".tbl_master_sediaan_bahan 
                        SET isdeleted = 1, dby = ? , ddt = now()
                        WHERE master_sediaan_bahan_id = ?";
                        
                $stmt = mysqli_prepare($this->dc, $qry);
                mysqli_stmt_bind_param($stmt, 'si', 
                                       $_SESSION['user_id'], 
                                       $data->master_sediaan_bahan_id);

                // sanitize    
                $data->master_sediaan_bahan_id=htmlspecialchars(strip_tags($data->master_sediaan_bahan_id));

                $exe = mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                
                if($exe) {                               
                    http_response_code(200); // set response code - 201 created 
                    echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
                } else {
                    
                    http_response_code(200); // set response code - 503 service unavailable
                    echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
                }      
            } else {    
                http_response_code(200); // set response code - 400 bad request
                echo json_encode(array("status" => NOT_OK, "message" => "Tidak dapat menghapus data. Stok sudah digunakan."));
            }                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
        
    function stokIsNotUsed($master_sediaan_bahan_id) {
        $qry = "select master_sediaan_bahan_id FROM tbl_stok_penerimaan WHERE master_sediaan_bahan_id = ". $master_sediaan_bahan_id ." AND isdeleted = 0";
        if($this->sqlNumRows($qry) > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    function get() {
        //####  DUPLICATE RECORD TO MIRROR TABLE EVERY TRX EXECUTE
        //echo "gogo\n";
        define("DBPSWD1", "Te\$t12356#1");//Test12356
        //for ($x = 0; $x <= 2685; $x++) {                      
            $mysqli = new mysqli("109.106.253.165","u1534565_appsparklink",DBPSWD1,"u1534565_dbsparklink");
            if ($mysqli -> connect_errno) {
              echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
              exit();
            }
            $sql = "select id_trx from trx where isdeleted=0 and sv1=0 order by id_trx asc limit 1";
            //echo $sql;return false;
            if ($result = $mysqli -> query($sql)) {
              while ($obj = $result -> fetch_object()) {
                  $q_dup="replace into trx_mirror
                  select * from trx where id_trx='".$obj->id_trx."' and isdeleted=0 and sv1=0";
                  //echo $q_dup."\n";
                  //return false;
                  if($mysqli -> query($q_dup)){
                      $qupd="update trx set sv1=1 where id_trx='".$obj->id_trx."' and isdeleted=0 and sv1=0";
                      echo $qupd."\n";
                      $mysqli -> query($qupd);
                  }
                //printf("%s (%s)\n", $obj->Lastname, $obj->Age);
              }
              $result -> free_result();
            }
            else{
                echo("Error description: " . $mysqli -> error);
            }
            $mysqli -> close();
            //echo "The number is: $x <br>";
//        }
        return false;
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->master_sediaan_bahan_id) && $data->master_sediaan_bahan_id != '') ? " AND m.master_sediaan_bahan_id = ". $data->master_sediaan_bahan_id : null;  
        $qry = "SELECT k.kategori kategori_barang, m.* FROM ".DBNAME.".tbl_master_sediaan_bahan m
                INNER JOIN ".DBNAME.".tbl_master_sediaan_bahan_kategori k ON m.kategori = k.master_sediaan_bahan_kategori_id 
                WHERE (m.puskesmas_id = ". $_SESSION['puskesmas_id'] ." OR m.iby = ". $_SESSION['user_id'] .") AND m.isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
}

$master = new mMaster();

switch( $master->a ) {
    
    case "post":
        $master->post();
    break;
    
    case "get":
        $master->get();
    break;   
    
    case "put":
        $master->put();
    break;
    
    case "delete":
        $master->delete();
    break;
}