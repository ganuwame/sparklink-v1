<?php

class mJukir extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                        
            !empty($data->jukir_name) &&
            !empty($data->jukir_phone) &&
            !empty($data->jukir_address) &&
            !empty($data->jukir_email) &&
            !empty($data->parking_location) &&
            !empty($data->device_uuid) &&
            !empty($data->jukir_code)
        ){
            
            $qry = "INSERT INTO ". DBNAME .".jukir 
                    (id_client,jukir_name, jukir_phone, jukir_address, jukir_email, id_parking_location, id_device, jukir_code, idt, iby) 
                    VALUES ('".$_SESSION['client_id']."', ?, ?, ?, ?, ?, ?, ?, now(), ?)";
            //echo $qry;return false;
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssssiisi',            
                                   $data->jukir_name, 
                                   $data->jukir_phone, 
                                   $data->jukir_address, 
                                   $data->jukir_email, 
                                   $data->parking_location,
                                   $data->device_uuid,
                                   $data->jukir_code,           
                                   $_SESSION['user_id']);

            // sanitize                        
            $data->jukir_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_name))));
            $data->jukir_phone = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_phone))));
            $data->jukir_address = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_address))));
            $data->jukir_email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_email))));
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));
            $data->device_uuid = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_uuid))));
            $data->jukir_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_code))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {
                $juk_id = mysqli_insert_id($this->dc);
                $uname = "jukir".$juk_id;
                $q_user = "INSERT INTO ". DBNAME .".tbl_user 
                        (id_client,name,username,
                        usergroup, passwd, uid, idt, iby) 
                        VALUES ('".$_SESSION['client_id']."','".$data->jukir_name."','$uname','4',md5('jukir123'), '$juk_id', now(),'".$_SESSION['user_id']."')";
                $this->execQuery($q_user);
                //####                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                    
            !empty($data->jukir_name) &&
            !empty($data->jukir_phone) &&
            !empty($data->jukir_address) &&
            !empty($data->jukir_email) &&
            !empty($data->parking_location) &&
            !empty($data->device_uuid) &&
            !empty($data->jukir_code) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".jukir 
                    SET jukir_name= ?, jukir_phone= ?, jukir_address= ?, jukir_email= ?, id_parking_location= ?, id_device= ?, jukir_code= ?, uby = ? , udt = now()
                    WHERE id_jukir = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssssiisii',                                    
                                   $data->jukir_name, 
                                   $data->jukir_phone, 
                                   $data->jukir_address, 
                                   $data->jukir_email, 
                                   $data->parking_location,
                                   $data->device_uuid,
                                   $data->jukir_code,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                                     
            $data->jukir_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_name))));
            $data->jukir_phone = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_phone))));
            $data->jukir_address = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_address))));
            $data->jukir_email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_email))));
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));
            $data->device_uuid = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_uuid))));
            $data->jukir_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_code))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".jukir 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_jukir = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {       
                $uname = "jukir".$data->id;
                $q_user = "update ". DBNAME .".tbl_user set isdeleted=1,ddt=now(),dby='".$_SESSION['user_id']."'  
                where id_client ='".$_SESSION['client_id']."' and username='".$uname."'
                        ";
                //echo $q_user;
                $this->execQuery($q_user);
                                        
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND a.id_jukir = ". $data->id : null;  
        $qry = "SELECT a.*,b.device_type,b.device_uuid,c.parking_location_name,d.username,d.id_user FROM ".DBNAME.".jukir a
        inner join ".DBNAME.".device b on a.id_device=b.id_device
        inner join ".DBNAME.".parking_location c on  a.id_parking_location=c.id_parking_location
        inner join ".DBNAME.".tbl_user d on a.id_jukir = d.uid and d.username like '%jukir%'
        WHERE a.id_client='".$_SESSION['client_id']."' and a.isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryDevice = "SELECT * FROM ". DBNAME .".device WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        $qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        //$qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataDevice = $this->selectQueryAsArray($qryDevice);
        $dataLocation = $this->selectQueryAsArray($qryLocation);
        //$dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataDevice" => $dataDevice, "dataLocation" => $dataLocation)); 
    }
    
}

$jukir = new mJukir();

switch( $jukir->a ) {
    
    case "post":
        $jukir->post();
    break;
    
    case "get":
        $jukir->get();
    break;   
    
    case "get-form-data":
        $jukir->getFormData();
    break; 
    
    case "put":
        $jukir->put();
    break;
    
    case "delete":
        $jukir->delete();
    break;
}