<?php

class mCheckInout extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                            
            !empty($data->id_jukir) &&
            !empty($data->id_parking_location) &&
            !empty($data->checkin_dt) &&
            !empty($data->checkin_long) &&
            !empty($data->checkin_lat) 
        ){
            
            $qry = "INSERT INTO ".DBNAME.".check_in_out 
                    (id_jukir, id_parking_location, checkin_dt, checkin_long, checkin_lat, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, now(), ?)"; 
            //cho $qry;return false;
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iisssi',//ssi        
                                   $data->id_jukir, 
                                   $data->id_parking_location, 
                                   $data->checkin_dt, 
                                   $data->checkin_long,
                                   $data->checkin_lat,
                                   $_SESSION['user_id']
                                   );

            // sanitize                                                                                                            
            $data->id_jukir = addslashes((htmlspecialchars(strip_tags($data->id_jukir))));
            $data->id_parking_location = addslashes((htmlspecialchars(strip_tags($data->id_parking_location))));
            $data->checkin_dt = addslashes((htmlspecialchars(strip_tags($data->checkin_dt))));
            $data->checkin_long = addslashes((htmlspecialchars(strip_tags($data->checkin_long))));    
            $data->checkin_lat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->checkin_lat))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {       
                $checkin_id = mysqli_insert_id($this->dc);                        
                http_response_code(200); // set response code - 201 created 
                echo json_encode(
                array(
                        "status" => OK, 
                        "message" => "Record was created.",
                        "id_check_in_out" => $checkin_id,
                        "check_in_dt" => $data->checkin_dt
                     )
                ); //SAVE_SUCCESS   
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));//SAVE_FAIL
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => SAVE_PARAMS_INVALID));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                                        
            !empty($data->checkout_dt) &&
            !empty($data->checkout_long) &&
            !empty($data->checkout_lat) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".check_in_out 
                    SET checkout_dt= ?, checkout_long= ?, checkout_lat= ?, uby = ? , udt = now()
                    WHERE id_check_in_out = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'sssii',  
                                   $data->checkout_dt, 
                                   $data->checkout_long, 
                                   $data->checkout_lat,                                    
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                                                 
            $data->checkout_dt = addslashes((htmlspecialchars(strip_tags($data->checkout_dt))));
            $data->checkout_long = addslashes((htmlspecialchars(strip_tags($data->checkout_long))));
            $data->checkout_lat = addslashes((htmlspecialchars(strip_tags($data->checkout_lat))));            
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));//EDIT_SUCCESS    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));//EDIT_FAIL
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => EDIT_PARAMS_INVALID));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".check_in_out 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_check_in_out = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_check_in_out = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".check_in_out WHERE isdeleted = 0" . $sql_str ;

        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryTingkat = "SELECT * FROM ". DBNAME .".tbl_tahfidz_tingkat WHERE isdeleted = 0";
        $qryHalaqah = "SELECT * FROM ". DBNAME .".tbl_tahfidz_halaqah WHERE isdeleted = 0";
        $qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataTingkat = $this->selectQueryAsArray($qryTingkat);
        $dataHalaqah = $this->selectQueryAsArray($qryHalaqah);
        $dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataTingkat" => $dataTingkat, "dataHalaqah" => $dataHalaqah, "dataKampus" => $dataKampus)); 
    }
    
}

$checkinout = new mCheckInout();

switch( $checkinout->a ) {
    
    case "post":
        $checkinout->post();
    break;
    
    case "get":
        $checkinout->get();
    break;
    
    case "get-form-data":
        $checkinout->getFormData();
    break;   
    
    case "put":
        $checkinout->put();
    break;
    
    case "delete":
        $checkinout->delete();
    break;
}