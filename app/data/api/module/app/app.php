<?php

class mSiswa extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->id_kampus) &&
            !empty($data->nis) &&
            !empty($data->nama) &&
            !empty($data->email) &&
            !empty($data->nohp)
        ){
            
            $qry = "INSERT INTO ". DBNAME .".tbl_tahfidz_siswa 
                    (id_kampus,nis,nama_siswa, siswa_email, siswa_nohp, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'issssi',
                                   $data->id_kampus,                                     
                                   $data->nis, 
                                   $data->nama, 
                                   $data->email, 
                                   $data->nohp,            
                                   $_SESSION['user_id']);

            // sanitize            
            $data->id_kampus = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_kampus))));
            $data->nis = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nis))));
            $data->nama = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nama))));
            $data->email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->email))));
            $data->nohp = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nohp))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->id_kampus) &&
            !empty($data->nis) &&
            !empty($data->nama) &&
            !empty($data->email) &&
            !empty($data->nohp) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_siswa 
                    SET id_kampus = ?, nis = ?,nama_siswa = ?, siswa_email = ?, siswa_nohp = ?, uby = ? , udt = now()
                    WHERE id_siswa = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iisssii', 
                                   $data->id_kampus, 
                                   $data->nis, 
                                   $data->nama, 
                                   $data->email, 
                                   $data->nohp,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->id_kampus = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_kampus))));
            $data->nis = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nis))));
            $data->nama = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nama))));
            $data->email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->email))));
            $data->nohp = addslashes(strtoupper(htmlspecialchars(strip_tags($data->nohp))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".tbl_tahfidz_siswa 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_siswa = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));                               
         
        $qryPopUp = "SELECT popup_id, content, button FROM ".DBNAME.".tbl_popup                
                     WHERE isdeleted = 0 AND (isshown = 0 OR isalways_show = 1) AND user_id=".$data->user_id."
                     ORDER BY popup_id, priority ASC LIMIT 1";
        $dataPopUp = $this->selectQueryAsObject($qryPopUp);                                   
         
        $qryNotification = "SELECT notification_id, title, content FROM ".DBNAME.".tbl_notification                
                            WHERE isdeleted = 0 AND isseen = 0 AND user_id=".$data->user_id;
                            
        $dataNotification = $this->selectQueryAsArray($qryNotification);
        
        http_response_code(200);
        $this->printJSON(json_encode(array("status" => OK, "dataNotification" => $dataNotification, "dataPopUp" => $dataPopUp))); 
    }
    
    function getDashboard() {
        //echo "Masuk bro";return false;
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));  
                                                                                                
        $qryClient = "select jukir_name,parking_location_name,client_name,sum(trx_daily) trx_daily,sum(trx_denom) trx_denom
                        from ".DBNAME.".agg_trx_daily_payment atdp
                        where id_client ='".$_SESSION['client_id']."'
                        group by jukir_name,parking_location_name ,client_name ";   
        $numClient = 0;          
        if($this->sqlNumRows($qryClient) > 0) {
            $dataClient = $this->selectQueryAsObject($qryClient); 
        }    
                                                                                                
        $qryJukir = "SELECT count(id_jukir) num FROM ".DBNAME.".jukir WHERE isdeleted = 0 and id_client='".$_SESSION['client_id']."'";   
        $numJukir = 0;          
        if($this->sqlNumRows($qryJukir) > 0) {
            $numJukir = $this->selectQueryAsObject($qryJukir)->num; 
        } 
        
        $qryIncomeBulan = "SELECT sum(trx_denom) num FROM ".DBNAME.".trx_mirror tm 
                        WHERE DATE_FORMAT(startdatetime,'%Y-%m')=DATE_FORMAT(now(),'%Y-%m') 
                        and id_client='".$_SESSION['client_id']."'
                        ";   
        $numIncomeBulan = 0;          
        if($this->sqlNumRows($qryIncomeBulan) > 0) {
            $numIncomeBulan = $this->selectQueryAsObject($qryIncomeBulan)->num; 
        } 
        
        $qryIncomeHari = "SELECT sum(trx_denom) num FROM ".DBNAME.".trx_mirror tm 
                        WHERE DATE_FORMAT(startdatetime,'%Y-%m-%d')=DATE_FORMAT(now(),'%Y-%m-%d') 
                        and id_client='".$_SESSION['client_id']."'
                        ";   
        $numIncomeHari = 0;          
        if($this->sqlNumRows($qryIncomeHari) > 0) {
            $numIncomeHari = $this->selectQueryAsObject($qryIncomeHari)->num; 
        }
                                                                                                      
        $qryLocation = "SELECT count(id_parking_location) num FROM ".DBNAME.".parking_location WHERE isdeleted = 0 and id_client='".$_SESSION['client_id']."'";   
        $numLocation = 0;          
        if($this->sqlNumRows($qryLocation) > 0) {
            $numLocation = $this->selectQueryAsObject($qryLocation)->num; 
        }
        //#### FOR MAP
        $dataArrLongLat = array();
        $labelArrLongLat = array("Lat","Long","Name");
        array_push($dataArrLongLat, $labelArrLongLat);
                
        $qryLatLongLok = "select parking_location_lat,parking_location_long,parking_location_name
                        from ".DBNAME.".parking_location  where id_client ='".$_SESSION['client_id']."'
                        ";   
        //$numLatLongLok = 0;          
        //if($this->sqlNumRows($qryLatLongLok) > 0) {
        $listLatLongLok = $this->selectQueryAsJSON($qryLatLongLok);//->num 
        
        $i=0;
        $rsLatLongLok = $this->execQuery($qryLatLongLok, true);
        while($rwLatLong = mysqli_fetch_array($rsLatLongLok)) { 
            $c = array();
            for($k=0;$k<=2;$k++){
                //echo $rwLatLong[$k]."\n";
                if($k==2){
                    array_push($c, $rwLatLong[$k]);
                }else{                    
                    array_push($c, floatval($rwLatLong[$k]));
                }                
            }
            array_push($dataArrLongLat, $c);
            //array_push($labelArrLocDay, $rwLatLong[]);
            $i++;
        }
            
        //}
        //####
        $labelArrLocDay = array("Month-Day");//,"MAJESTIK GATOT SUBROTO","MAJESTIK KAPTEN MUSLIM UPD"
        $qlocdayhead ="select DISTINCT (a.parking_location_name) from
                (
                select startdate,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) denom from agg_trx_daily_ride 
                where id_client='".$_SESSION['client_id']."' and startdate BETWEEN DATE_SUB(NOW() - INTERVAL 1 DAY, INTERVAL 30 DAY) AND NOW()
                group by startdate,parking_location_name
                ) a
                order by a.parking_location_name";
        $i=0;
        $rslocday = $this->execQuery($qlocdayhead, true);
        while($rw = mysqli_fetch_array($rslocday)) {            
            array_push($labelArrLocDay, $rw['parking_location_name']);
            $i++;
        }
        //###
        $dataArrLocDay = array();
        array_push($dataArrLocDay, $labelArrLocDay);
        //######### PARKING LOCATION DAILY REV
        $qlocday = "select DISTINCT (a.startdate),DATE_FORMAT(a.startdate,'%Y%m%d') sdate from
                (
                select startdate,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' and startdate BETWEEN DATE_SUB(NOW() - INTERVAL 1 DAY, INTERVAL 30 DAY) AND NOW()
                group by startdate,parking_location_name
                ) a
                order by a.startdate asc
                ";
        $rslocday = $this->execQuery($qlocday, true);
        //$dataLoc = $this->selectQueryAsObject($qry); 
        //$dataArr2 = [];
        $i=0;
        while($rw = mysqli_fetch_array($rslocday)) {
            $b=array();
            array_push($b, $rw['sdate']);
            //array_push($labelArr, $rw['parking_location_name']);
            //####
            $qhead ="select DISTINCT (a.parking_location_name) from
                (
                select startdate,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) denom from agg_trx_daily_ride 
                where id_client='".$_SESSION['client_id']."' and startdate BETWEEN DATE_SUB(NOW() - INTERVAL 1 DAY, INTERVAL 30 DAY) AND NOW()
                group by startdate,parking_location_name
                ) a
                order by a.parking_location_name";
            $rshead = $this->execQuery($qhead, true);
            //$i=0;
            while($rwhead = mysqli_fetch_array($rshead)) {
                //$a=array();
                //array_push($labelArr, $rw['parking_location_name']);
                $qbody ="select a.denom from
                (
                select startdate,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) denom from agg_trx_daily_ride 
                where id_client='".$_SESSION['client_id']."' and startdate BETWEEN DATE_SUB(NOW() - INTERVAL 1 DAY, INTERVAL 30 DAY) AND NOW()
                group by startdate,parking_location_name
                ) a
                where a.parking_location_name='".$rwhead['parking_location_name']."' and a.startdate='".$rw['startdate']."'";
                $rsbody = $this->execQuery($qbody, true);
                $i=0;
                while($rwbody = mysqli_fetch_array($rsbody)) {
                    array_push($b, (int)$rwbody['denom']);
                }                                            
            }           
            array_push($dataArrLocDay, $b);
            $i++;
        }
        //#########
        
        //######## REVENUE BULANAN
        //######### PARKING LOCATION DAILY REV
        $labelArrLocDay2 = array("Year-Month");
        $qlocdayhead2 ="select DISTINCT (a.parking_location_name) from
                (
                    select a.bulan,a.parking_location_name,sum(a.trx_denom) trx_denom  from
                    (
                    select date_format(startdate,'%Y-%m') bulan,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                    from ".DBNAME.".agg_trx_daily_ride 
                    where id_client ='".$_SESSION['client_id']."' 
                    group by bulan,parking_location_name                        
                    ) a
                    group by a.bulan,a.parking_location_name
                ) a
                order by a.parking_location_name";
        $i=0;
        $rslocday2 = $this->execQuery($qlocdayhead2, true);
        while($rw2 = mysqli_fetch_array($rslocday2)) {            
            array_push($labelArrLocDay2, $rw2['parking_location_name']);
            $i++;
        }
        $dataArrLocDay2 = array();
        array_push($dataArrLocDay2, $labelArrLocDay2);
        $qlocday2 = "select DISTINCT(b.bulan) from (
                select a.bulan,a.parking_location_name,sum(a.trx_denom) trx_denom  from
                (
                select date_format(startdate,'%Y-%m') bulan,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' 
                group by bulan,parking_location_name                        
                ) a
                group by a.bulan,a.parking_location_name
                ) b                
                ";
        $rslocday2 = $this->execQuery($qlocday2, true);
        //$dataLoc = $this->selectQueryAsObject($qry); 
        //$dataArr2 = [];
        $i=0;
        while($rw2 = mysqli_fetch_array($rslocday2)) {
            $b2=array();
            array_push($b2, $rw2['bulan']);
            //array_push($labelArr, $rw['parking_location_name']);
            //####
            $qhead2 ="select DISTINCT (b.parking_location_name) from
                (
                select a.bulan,a.parking_location_name,sum(a.trx_denom) trx_denom  from
                (
                select date_format(startdate,'%Y-%m') bulan,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' 
                group by bulan,parking_location_name                        
                ) a
                group by a.bulan,a.parking_location_name
                ) b
                order by b.parking_location_name";
            $rshead2 = $this->execQuery($qhead2, true);
            //$i=0;
            while($rwhead2 = mysqli_fetch_array($rshead2)) {
                //$a=array();
                //array_push($labelArr, $rw['parking_location_name']);
                $qbody2 ="select b.denom from
                (
                    select a.bulan,a.parking_location_name,sum(a.trx_denom) denom  from
                    (
                    select date_format(startdate,'%Y-%m') bulan,parking_location_name,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                    from ".DBNAME.".agg_trx_daily_ride 
                    where id_client ='".$_SESSION['client_id']."' 
                    group by bulan,parking_location_name                        
                    ) a
                    group by a.bulan,a.parking_location_name                                
                ) b
                where b.parking_location_name='".$rwhead2['parking_location_name']."' and b.bulan='".$rw2['bulan']."'";
                $rsbody2 = $this->execQuery($qbody2, true);
                $i=0;
                while($rwbody2 = mysqli_fetch_array($rsbody2)) {
                    array_push($b2, (int)$rwbody2['denom']);
                }                                            
            }           
            array_push($dataArrLocDay2, $b2);
            $i++;
        }
        //#########
        //######### PARKING LOCATION DAILY REV
        $labelArrRideMonth = array("Year-Month");
        $qRideMonthhead ="select DISTINCT(b.vehicle_type) from (    
                select a.bulan,a.vehicle_type,sum(a.trx_denom) trx_denom  from
                (
                select date_format(startdate,'%Y-%m') bulan,vehicle_type,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' 
                group by bulan,vehicle_type                        
                ) a
                group by a.bulan,a.vehicle_type
                ) b
                order by b.vehicle_type asc    
                ";
        $i=0;
        $rsRideMonth = $this->execQuery($qRideMonthhead, true);
        while($rw3 = mysqli_fetch_array($rsRideMonth)) {            
            array_push($labelArrRideMonth, $rw3['vehicle_type']);
            $i++;
        }
        $dataArrRideMonth = array();
        array_push($dataArrRideMonth, $labelArrRideMonth);
        $qMonth = "select DISTINCT(b.bulan) from (
                select a.bulan,a.vehicle_type,sum(a.trx_denom) trx_denom  from
                (
                select date_format(startdate,'%Y-%m') bulan,vehicle_type,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' 
                group by bulan,vehicle_type                        
                ) a
                group by a.bulan,a.vehicle_type
                ) b                
                ";
        $rsMonth = $this->execQuery($qMonth, true);
        //$dataLoc = $this->selectQueryAsObject($qry); 
        //$dataArr2 = [];
        $i=0;
        while($rwMonth = mysqli_fetch_array($rsMonth)) {
            $b3=array();
            array_push($b3, $rwMonth['bulan']);
            //array_push($labelArr, $rw['parking_location_name']);
            //####
            $qhead3 ="select DISTINCT (b.vehicle_type) from
                (
                select a.bulan,a.vehicle_type,sum(a.trx_denom) trx_denom  from
                (
                select date_format(startdate,'%Y-%m') bulan,vehicle_type,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                from ".DBNAME.".agg_trx_daily_ride 
                where id_client ='".$_SESSION['client_id']."' 
                group by bulan,vehicle_type                        
                ) a
                group by a.bulan,a.vehicle_type
                ) b
                order by b.vehicle_type";
            $rshead3 = $this->execQuery($qhead3, true);
            //$i=0;
            while($rwhead3 = mysqli_fetch_array($rshead3)) {
                //$a=array();
                //array_push($labelArr, $rw['parking_location_name']);
                $qbody3 ="select b.denom from
                (
                    select a.bulan,a.vehicle_type,sum(a.trx_denom) denom  from
                    (
                    select date_format(startdate,'%Y-%m') bulan,vehicle_type,sum(trx_daily) tdaily,sum(trx_denom) trx_denom 
                    from ".DBNAME.".agg_trx_daily_ride 
                    where id_client ='".$_SESSION['client_id']."' 
                    group by bulan,vehicle_type                        
                    ) a
                    group by a.bulan,a.vehicle_type                                
                ) b
                where b.vehicle_type='".$rwhead3['vehicle_type']."' and b.bulan='".$rwMonth['bulan']."'";
                $rsbody3 = $this->execQuery($qbody3, true);
                $i=0;
                while($rwbody3 = mysqli_fetch_array($rsbody3)) {
                    array_push($b3, (int)$rwbody3['denom']);
                }                                            
            }           
            array_push($dataArrRideMonth, $b3);
            $i++;
        }
        //#########
        //#######
        $qry = "select parking_location_name,sum(trx_denom) trx_denom
                        from ".DBNAME.".agg_trx_daily_payment atdp
                        where id_client ='".$_SESSION['client_id']."'
                        group by jukir_name,parking_location_name ,client_name ";
        $rs = $this->execQuery($qry, true);
        //$dataLoc = $this->selectQueryAsObject($qry); 
        
        $labelArr = array("Location Name","Denom");
        $dataArr = array();
        array_push($dataArr, $labelArr);
        //$dataArr2 = [];
        $i=0;
        while($rw = mysqli_fetch_array($rs)) {
            $a=array();
            //array_push($labelArr, $rw['parking_location_name']);
            for($k=0;$k<=1;$k++){
                if($k==0){
                    array_push($a, $rw[$k]);
                }else{
                    array_push($a, (int)$rw[$k]);
                }                
            }
            array_push($dataArr, $a);
            $i++;
        }
        
        http_response_code(200);
        $this->printJSON(
            json_encode(
                array(
                    "status" => OK, 
                    "dataDashboard" => array(
                        "dataClient" => $dataClient, 
                        "numJukir" => $numJukir, 
                        "numLokasi" => $numLocation,
                        "numIncomeHariIni" => $numIncomeHari,
                        "numIncomeBulanIni" => $numIncomeBulan
                    ),
                    "dataChart" => array(
                        "label" => $labelArr,
                        "data" => $dataArr,
                        "locdaylabel" => $labelArrLocDay,
                        "chartlocday" => $dataArrLocDay,
                        "chartlocmonth" => $dataArrLocDay2,
                        "chartMap" => $dataArrLongLat,
                        "chartRideMonth" => $dataArrRideMonth
                    )
                )
            )
        ); 
    }
    
}

$siswa = new mSiswa();

switch( $siswa->a ) {
    
    case "post":
        $siswa->post();
    break;
    
    case "get":
        $siswa->get();
    break;   
    
    case "get-dashboard":
        $siswa->getDashboard();
    break;
    
    case "put":
        $siswa->put();
    break;
    
    case "delete":
        $siswa->delete();
    break;
}