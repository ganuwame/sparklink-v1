<?php

class mTrx extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->id_jukir) &&
            !empty($data->id_parking_location) &&
            !empty($data->id_vehicle_type) &&
            !empty($data->trx_dt) &&
            !empty($data->id_payment_type)             
        ){
            
            $qry = "INSERT INTO ". DBNAME.".trx 
                    (id_jukir, id_parking_location, id_vehicle_type, startdatetime, id_payment_type, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiisii',
                                   $data->id_jukir,                                     
                                   $data->id_parking_location, 
                                   $data->id_vehicle_type, 
                                   $data->trx_dt, 
                                   $data->id_payment_type,                                                                 
                                   $_SESSION['user_id']);

            // sanitize            
            $data->id_jukir = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_jukir))));
            $data->id_parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_parking_location))));
            $data->id_vehicle_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_vehicle_type))));
            $data->trx_dt = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_dt))));
            $data->id_payment_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_payment_type))));
            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                    
                $trx_id = mysqli_insert_id($this->dc);               
                $q_tingkat="select a.id_trx,b.jukir_name,b.jukir_code,c.payment_type,d.parking_location_name,d.parking_location_code,
                        e.vehicle_type,f.client_name,f.client_code,g.tarif_name,g.tarif_denom as trx_denom,h.trx_type,f.id_client
                        from ". DBNAME .".trx a
                        inner join ". DBNAME .".jukir b on a.id_jukir = b.id_jukir
                        inner join ". DBNAME .".client f on f.id_client = b.id_client
                        inner join ". DBNAME .".payment_type c on c.id_payment_type = a.id_payment_type
                        inner join ". DBNAME .".parking_location d on d.id_parking_location = a.id_parking_location
                        inner join ". DBNAME .".vehicle_type e on e.id_vehicle_type = a.id_vehicle_type
                        inner join ". DBNAME .".tarif g on g.id_vehicle_type = a.id_vehicle_type
                        inner join ". DBNAME .".trx_type h on d.id_trx_type = h.id_trx_type
                        where a.isdeleted=0
                        and a.id_trx='".$trx_id."'";
                //echo $q_tingkat;
                $rs=mysqli_query($this->dc,$q_tingkat);
                $rwtkt = mysqli_fetch_object($rs);
                //print_r($rwtkt);
                //echo "<BR>TEST<BR>".$rwtkt['tingkat_id'];
                $q_rest="update ". DBNAME .".trx set jukir_name='".$rwtkt->jukir_name."',jukir_code='".$rwtkt->jukir_code."', 
                payment_type='".$rwtkt->payment_type."',parking_location_name='".$rwtkt->parking_location_name."',
                parking_location_code='".$rwtkt->parking_location_code."',vehicle_type='".$rwtkt->vehicle_type."',
                client_name='".$rwtkt->client_name."',client_code='".$rwtkt->client_code."',
                trx_denom='".$rwtkt->trx_denom."',trx_type='".$rwtkt->trx_type."',id_client='".$rwtkt->id_client."'
                where id_trx='".$rwtkt->id_trx."' and isdeleted=0";
                //echo $q_rest;
                $this->execQuery($q_rest);
                //#####           
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->id_jukir) &&
            !empty($data->id_parking_location) &&
            !empty($data->id_vehicle_type) &&
            !empty($data->trx_dt) &&
            !empty($data->id_payment_type) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".trx 
                    SET id_jukir= ?, id_parking_location= ?, id_vehicle_type= ?, startdatetime= ?, id_payment_type= ?, uby = ? , udt = now()
                    WHERE id_trx = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iiisiii', 
                                   $data->id_jukir,                                     
                                   $data->id_parking_location, 
                                   $data->id_vehicle_type, 
                                   $data->trx_dt, 
                                   $data->id_payment_type,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->id_jukir = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_jukir))));
            $data->id_parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_parking_location))));
            $data->id_vehicle_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_vehicle_type))));
            $data->trx_dt = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_dt))));
            $data->id_payment_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->id_payment_type))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {
                $q_tingkat="select a.id_trx,b.jukir_name,b.jukir_code,c.payment_type,d.parking_location_name,d.parking_location_code,
                        e.vehicle_type,f.client_name,f.client_code,g.tarif_name,g.tarif_denom as trx_denom,h.trx_type,f.id_client
                        from ". DBNAME .".trx a
                        inner join ". DBNAME .".jukir b on a.id_jukir = b.id_jukir
                        inner join ". DBNAME .".client f on f.id_client = b.id_jukir
                        inner join ". DBNAME .".payment_type c on c.id_payment_type = a.id_payment_type
                        inner join ". DBNAME .".parking_location d on d.id_parking_location = a.id_parking_location
                        inner join ". DBNAME .".vehicle_type e on e.id_vehicle_type = a.id_vehicle_type
                        inner join ". DBNAME .".tarif g on g.id_vehicle_type = a.id_vehicle_type
                        inner join ". DBNAME .".trx_type h on d.id_trx_type = h.id_trx_type
                        where a.isdeleted=0
                        and a.id_trx='".$data->id."'";
                //echo $q_tingkat;
                $rs=mysqli_query($this->dc,$q_tingkat);
                $rwtkt = mysqli_fetch_object($rs);
                //print_r($rwtkt);
                //echo "<BR>TEST<BR>".$rwtkt['tingkat_id'];
                $q_rest="update ". DBNAME .".trx set jukir_name='".$rwtkt->jukir_name."',jukir_code='".$rwtkt->jukir_code."', 
                payment_type='".$rwtkt->payment_type."',parking_location_name='".$rwtkt->parking_location_name."',
                parking_location_code='".$rwtkt->parking_location_code."',vehicle_type='".$rwtkt->vehicle_type."',
                client_name='".$rwtkt->client_name."',client_code='".$rwtkt->client_code."',
                trx_denom='".$rwtkt->trx_denom."',trx_type='".$rwtkt->trx_type."',id_client='".$rwtkt->id_client."'
                where id_trx='".$rwtkt->id_trx."' and isdeleted=0";
                //echo $q_rest;
                $this->execQuery($q_rest);    
                //###                                            
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".trx 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_trx = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_trx = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".trx WHERE isdeleted = 0" . $sql_str ." limit 30" ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getTrxDaily() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
        if(        
            !empty($data->id_jukir) ||
            !empty($data->trx_denom) ||
            !empty($data->hari) 
        ){
            $sql_str ="";
            $sql_str .= (isset($data->id_jukir) && $data->id_jukir != '') ? " AND id_jukir = '". $data->id_jukir."'" : null;  
            $sql_str .= (isset($data->trx_denom) && $data->trx_denom != '') ? " AND trx_denom = '". $data->trx_denom."'" : null;  
            $sql_str .= (isset($data->hari) && $data->hari != '') ? " AND DATE_FORMAT(startdatetime,'%Y-%m-%d') = '". $data->hari."'" : null;  
            
            $qry = "SELECT id_trx,jukir_name,parking_location_name,vehicle_type,trx_denom,startdatetime
                    FROM ".DBNAME.".trx WHERE isdeleted = 0" . $sql_str ." limit 50" ;
            //echo $qry;return false; //id_trx,jukir_name,parking_location_name,vehicle_type,trx_denom,startdatetime 
            if($this->sqlNumRows($qry) > 0) {
                $numRecords = $this->sqlNumRows($qry);
                $dataRecords = $this->selectQueryAsArray($qry);
                http_response_code(200);
                $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                            
            } else {                                     
                http_response_code(200);
                echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
            }
            
        }
        else{
            $qry = "SELECT id_trx,jukir_name,parking_location_name,vehicle_type,trx_denom,startdatetime
                    FROM ".DBNAME.".trx WHERE isdeleted = 0 limit 50" ;
            //echo $qry;return false; //id_trx,jukir_name,parking_location_name,vehicle_type,trx_denom,startdatetime 
            if($this->sqlNumRows($qry) > 0) {
                $numRecords = $this->sqlNumRows($qry);
                $dataRecords = $this->selectQueryAsArray($qry);
                http_response_code(200);
                $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                            
            } else {                                     
                http_response_code(200);
                echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
            }
        }
        //$sql_str = (isset($data->id) && $data->id != '') ? " AND id_trx = ". $data->id : null;  
        
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryTarif = "select t.id_tarif,t.id_vehicle_type,t.tarif_denom,concat(v.vehicle_type,' [',t.tarif_denom,']') ride_cost
                        from ". DBNAME .".tarif t
                        inner join ". DBNAME .".vehicle_type v on v.id_vehicle_type = t.id_vehicle_type
                        WHERE t.id_client='".$_SESSION['client_id']."' and t.isdeleted = 0
                        ";
        $qryLocation = "SELECT id_parking_location, parking_location_name,parking_location_code 
                FROM ". DBNAME .".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        $qryJukir = "SELECT * FROM ". DBNAME .".jukir WHERE isdeleted = 0 and id_client='".$_SESSION['client_id']."'";
        //$qryHalaqah = "SELECT * FROM ". DBNAME .".tbl_tahfidz_halaqah WHERE isdeleted = 0";
//        $qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataJukir = $this->selectQueryAsArray($qryJukir);
        $dataTarif = $this->selectQueryAsArray($qryTarif);
        $dataLocation = $this->selectQueryAsArray($qryLocation);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataJukir" => $dataJukir, "dataTarif" => $dataTarif, "dataLocation" => $dataLocation)); 
    }
    
}

$trx = new mTrx();

switch( $trx->a ) {
    
    case "post":
        $trx->post();
    break;
    
    case "get":
        $trx->get();
    break; 
    
    case "get-trx-daily":
        $trx->getTrxDaily();
    break;  
    
    case "get-form-data":
        $trx->getFormData();
    break;
    
    case "put":
        $trx->put();
    break;
    
    case "delete":
        $trx->delete();
    break;
}