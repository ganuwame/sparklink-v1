<?php

class mDevice extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->device_code) &&
            !empty($data->device_uuid) &&            
            !empty($data->device_type) ||
            !empty($data->jukir_id) 
            
        ){
            
            $qry = "INSERT INTO ". DBNAME .".device 
                    (device_code, device_uuid, id_client, id_jukir, device_type, idt, iby) 
                    VALUES (?, ?, '".$_SESSION['client_id']."', ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssisi', 
                                   $data->device_code, 
                                   $data->device_uuid,                                                
                                   $data->jukir_id,
                                   $data->device_type,
                                   $_SESSION['user_id']);

            // sanitize
            $data->device_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_code))));
            $data->device_uuid = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_uuid))));            
            $data->jukir_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_id))));
            $data->device_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_type))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->device_code) &&
            !empty($data->device_uuid) &&                        
            !empty($data->device_type) &&
            !empty($data->id) ||
            !empty($data->jukir_id) 
        ){            
            $qry = "UPDATE ". DBNAME .".device 
                    SET device_code= ?, device_uuid= ?, id_jukir= ?, device_type= ?, uby = ? , udt = now()
                    WHERE id_device = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssisii', 
                                   $data->device_code, 
                                   $data->device_uuid,                                               
                                   $data->jukir_id,
                                   $data->device_type,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->device_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_code))));
            $data->device_uuid = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_uuid))));            
            $data->jukir_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->jukir_id))));
            $data->device_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->device_type))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".device 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_device = ? and id_client='".$_SESSION['client_id']."'";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'si', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->device_id) && $data->device_id != '') ? " AND a.id_device = ". $data->device_id : null;  
        $qry = "SELECT a.* FROM ".DBNAME.".device a        
        WHERE a.id_client='".$_SESSION['client_id']."' and a.isdeleted = 0" . $sql_str ;//inner join jukir b on a.id_jukir=b.id_jukir
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryJukir = "SELECT * FROM ". DBNAME .".jukir WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        //$qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        //$qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataJukir = $this->selectQueryAsArray($qryJukir);
        //$dataLocation = $this->selectQueryAsArray($qryLocation);
        //$dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataJukir" => $dataJukir)); 
    }
    
}

$device = new mDevice();

switch( $device->a ) {
    
    case "post":
        $device->post();
    break;
    
    case "get":
        $device->get();
    break;   
    
    case "get-form-data":
        $device->getFormData();
    break;
    
    case "put":
        $device->put();
    break;
    
    case "delete":
        $device->delete();
    break;
}