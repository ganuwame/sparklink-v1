<?php

class mParkingLocation extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                        
            !empty($data->parking_location_name) &&
            !empty($data->parking_location_code) &&
            !empty($data->parking_location_long) &&
            !empty($data->parking_location_lat) &&
            !empty($data->trx_type) 
        ){
            
            $qry = "INSERT INTO ". DBNAME.".parking_location 
                    (id_client,parking_location_name, parking_location_code, parking_location_long, parking_location_lat, id_trx_type, idt, iby) 
                    VALUES ('".$_SESSION['client_id']."', ?, ?, ?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssssii',            
                                   $data->parking_location_name, 
                                   $data->parking_location_code, 
                                   $data->parking_location_long, 
                                   $data->parking_location_lat, 
                                   $data->trx_type,                                   
                                   $_SESSION['user_id']);

            // sanitize                        
            $data->parking_location_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_name))));
            $data->parking_location_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_code))));
            $data->parking_location_long = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_long))));
            $data->parking_location_lat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_lat))));
            $data->trx_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_type))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                    
            !empty($data->parking_location_name) &&
            !empty($data->parking_location_code) &&
            !empty($data->parking_location_long) &&
            !empty($data->parking_location_lat) &&
            !empty($data->trx_type) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".parking_location 
                    SET parking_location_name= ?, parking_location_code= ?, parking_location_long= ?, parking_location_lat= ?, id_trx_type= ?, uby = ? , udt = now()
                    WHERE id_parking_location = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssssiii',                                    
                                   $data->parking_location_name, 
                                   $data->parking_location_code, 
                                   $data->parking_location_long, 
                                   $data->parking_location_lat, 
                                   $data->trx_type,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                                     
            $data->parking_location_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_name))));
            $data->parking_location_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_code))));
            $data->parking_location_long = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_long))));
            $data->parking_location_lat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location_lat))));
            $data->trx_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->trx_type))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".parking_location 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_parking_location = ? and id_client='".$_SESSION['client_id']."'";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_parking_location = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryTarifType = "SELECT * FROM ". DBNAME .".trx_type WHERE isdeleted = 0";
        //$qryHalaqah = "SELECT * FROM ". DBNAME .".tbl_tahfidz_halaqah WHERE isdeleted = 0";
//        $qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataTarifType = $this->selectQueryAsArray($qryTarifType);
        //$dataHalaqah = $this->selectQueryAsArray($qryHalaqah);
//        $dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataTarifType" => $dataTarifType)); 
    }
    
}

$parkinglocation = new mParkingLocation();

switch( $parkinglocation->a ) {
    
    case "post":
        $parkinglocation->post();
    break;
    
    case "get":
        $parkinglocation->get();
    break;   
    
    case "get-form-data":
        $parkinglocation->getFormData();
    break;
    
    case "put":
        $parkinglocation->put();
    break;
    
    case "delete":
        $parkinglocation->delete();
    break;
}