<?php

class mPaymentType extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->payment_type)               
        ){
            
            $qry = "INSERT INTO ". DBNAME.".payment_type 
                    (payment_type, idt, iby) 
                    VALUES (?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'si',
                                   $data->payment_type,                                                                        
                                   $_SESSION['user_id']);

            // sanitize            
            $data->payment_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->payment_type))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->payment_type) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".payment_type 
                    SET payment_type= ?, uby = ? , udt = now()
                    WHERE id_payment_type = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'sii', 
                                   $data->payment_type,                                                                        
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->payment_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->payment_type))));            
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".payment_type 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_payment_type = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_payment_type = ". $data->id : null;  
        $qry = "SELECT * FROM ".DBNAME.".payment_type WHERE isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
}

$paymenttype = new mPaymentType();

switch( $paymenttype->a ) {
    
    case "post":
        $paymenttype->post();
    break;
    
    case "get":
        $paymenttype->get();
    break;   
    
    case "put":
        $paymenttype->put();
    break;
    
    case "delete":
        $paymenttype->delete();
    break;
}