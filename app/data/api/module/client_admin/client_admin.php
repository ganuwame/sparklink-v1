<?php

class mClientAdmin extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->admin_name) &&
            !empty($data->client_id)
        ){
            
            $qry = "INSERT INTO ". DBNAME .".client_admin 
                    (admin_name, id_client, idt, iby) 
                    VALUES (?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'sii', 
                                   $data->admin_name, 
                                   $data->client_id,             
                                   $_SESSION['user_id']);

            // sanitize
            $data->admin_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->admin_name))));
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {  
                $adm_id = mysqli_insert_id($this->dc);
                $uname = "admin".$adm_id;
                $q_user = "INSERT INTO ". DBNAME .".tbl_user 
                        (id_client,name,username,
                        usergroup, passwd, uid, idt, iby) 
                        VALUES ('".$data->client_id."','".$data->admin_name."','$uname','3',md5('admin123'), '$adm_id', now(),'".$_SESSION['user_id']."')";
                //echo $q_user;
                $this->execQuery($q_user);                    
                // GEN HISTORY
                /*$q_tingkat="select tingkat_id from ". DBNAME .".tbl_tahfidz_halaqah where halaqah_id='".$data->halaqah."' and isdeleted=0";
                //echo $q_tingkat;
                $rs=mysqli_query($this->dc,$q_tingkat);
                $rwtkt = mysqli_fetch_object($rs);
                //print_r($rwtkt);
                //echo "<BR>TEST<BR>".$rwtkt['tingkat_id'];
                //$hal_id = mysqli_insert_id($this->dc);                         
                $q_hist="insert into ". DBNAME .".tbl_tahfidz_tingkat_siswa_history set siswa_id='".$data->siswa_id."',
                        tingkat_id='".$rwtkt->tingkat_id."',halaqah_id='".$data->halaqah."',iby='".$_SESSION['user_id']."',idt=now();                        
                        ";
                //echo $q_hist;
                $q_rest="update ". DBNAME .".tbl_tahfidz_siswa set tingkat_id='".$rwtkt->tingkat_id."',halaqah_id='".$data->halaqah."' where siswa_id='".$data->siswa_id."' and isdeleted=0";
                $this->execQuery($q_hist);
                $this->execQuery($q_rest);*/
                // ####                
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->admin_name) &&
            !empty($data->client_id) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".client_admin 
                    SET admin_name = ?, id_client = ?, uby = ? , udt = now()
                    WHERE id_client_admin = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'siii', 
                                   $data->admin_name, 
                                   $data->client_id, 
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->admin_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->admin_name))));
            $data->client_id = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_id))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) &&
            !empty($data->client_id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".client_admin 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_client_admin = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));
            
            // RELEASE SISWA TO FREE
                /*$q_siswa="select siswa_id from ". DBNAME .".client_admin where halaqah_siswa_list_id='".$data->id."' and isdeleted=0";                
                //echo $q_siswa;
                $rs=mysqli_query($this->dc,$q_siswa);
                $rwsws = mysqli_fetch_object($rs);
                //print_r($rwsws);//return false;
                //var_dump($rwsws);return false;
                $q_release="update ". DBNAME .".tbl_tahfidz_siswa set tingkat_id=0,halaqah_id=0 
                            where siswa_id='".$rwsws->siswa_id."' and isdeleted=0";
                $this->execQuery($q_release); 
                $q_zero="update ". DBNAME .".tbl_tahfidz_tingkat_siswa_history set iscurrent=0 
                where siswa_id='".$rwsws->siswa_id."' and iscurrent=1 and isdeleted=0";
                $this->execQuery($q_zero); */                             
            //####

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {         
                $uname = "admin".$data->id;
                $q_user = "update ". DBNAME .".tbl_user set isdeleted=1,ddt=now(),dby='".$_SESSION['user_id']."'  
                where id_client ='".$data->client_id."' and username='".$uname."'
                        ";
                //echo $q_user;
                $this->execQuery($q_user);
                       
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND a.id_client_admin = ". $data->id : null;  
        $qry = "SELECT a.*,b.client_name,c.username,c.id_user FROM ".DBNAME.".client_admin a
        inner join client b on a.id_client=b.id_client
        inner join tbl_user c on a.id_client_admin =c.uid and c.username like 'admin%'
        WHERE a.isdeleted = 0" . $sql_str ;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryClient = "SELECT * FROM ". DBNAME .".client WHERE isdeleted = 0";
        //$qryHalaqah = "SELECT * FROM ". DBNAME .".tbl_tahfidz_halaqah WHERE isdeleted = 0";
//        $qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataClient = $this->selectQueryAsArray($qryClient);
        //$dataHalaqah = $this->selectQueryAsArray($qryHalaqah);
//        $dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataClient" => $dataClient)); 
    }
    
}

$clientadmin = new mClientAdmin();

switch( $clientadmin->a ) {
    
    case "post":
        $clientadmin->post();
    break;
    
    case "get":
        $clientadmin->get();
    break;   
    
    case "get-form-data":
        $clientadmin->getFormData();
    break;
    // DISABLE KARENA TIDAK DIPERLUKAN
    case "put":
        $clientadmin->put();
    break;
    
    // USE FOR RELEASE SISWA NAIK TIMGKAT MISALNYA
    case "delete":
        $clientadmin->delete();
    break;
}