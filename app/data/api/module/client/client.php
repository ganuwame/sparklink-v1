<?php

class mClient extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->client_name) &&
            !empty($data->client_code) &&
            !empty($data->address) &&
            !empty($data->phone) &&
            !empty($data->email) ||
            !empty($data->office_long) ||
            !empty($data->office_lat)             
        ){
            
            $qry = "INSERT INTO ". DBNAME .".client 
                    (client_name, address, phone,email,office_long,office_lat,client_code, idt, iby) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'sssssssi', 
                                   $data->client_name, 
                                   $data->address, 
                                   $data->phone,            
                                   $data->email,            
                                   $data->office_long,            
                                   $data->office_lat,            
                                   $data->client_code,            
                                   $_SESSION['user_id']);

            // sanitize
            $data->client_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_name))));
            $data->address = addslashes(strtoupper(htmlspecialchars(strip_tags($data->address))));
            $data->phone = addslashes(strtoupper(htmlspecialchars(strip_tags($data->phone))));
            $data->email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->email))));
            $data->office_long = addslashes(strtoupper(htmlspecialchars(strip_tags($data->office_long))));
            $data->office_lat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->office_lat))));
            $data->client_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_code))));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {
                $cln_id = mysqli_insert_id($this->dc);
                $uname = "client".$cln_id;
                $q_user = "INSERT INTO ". DBNAME .".tbl_user 
                        (id_client,name,username,
                        usergroup, passwd, uid, idt, iby) 
                        VALUES ('$cln_id','$data->client_name','$uname','2',md5('owner123'),'$cln_id', now(),'".$_SESSION['user_id']."')";
                //echo $q_user;
                $this->execQuery($q_user);                                       
                //###
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->client_name) &&
            !empty($data->client_code) &&
            !empty($data->address) &&
            !empty($data->phone) &&
            !empty($data->id) ||
            !empty($data->office_long) ||
            !empty($data->office_lat) ||
            !empty($data->email)            
        ){            
            $qry = "UPDATE ". DBNAME .".client 
                    SET client_name= ?, address= ?, phone= ?,email= ?,office_long= ?,office_lat= ?,client_code= ?, uby = ? , udt = now()
                    WHERE id_client = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'sssssssii', 
                                   $data->client_name, 
                                   $data->address, 
                                   $data->phone,            
                                   $data->email,            
                                   $data->office_long,            
                                   $data->office_lat,            
                                   $data->client_code,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->client_name = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_name))));
            $data->address = addslashes(strtoupper(htmlspecialchars(strip_tags($data->address))));
            $data->phone = addslashes(strtoupper(htmlspecialchars(strip_tags($data->phone))));
            $data->email = addslashes(strtoupper(htmlspecialchars(strip_tags($data->email))));
            $data->office_long = addslashes(strtoupper(htmlspecialchars(strip_tags($data->office_long))));
            $data->office_lat = addslashes(strtoupper(htmlspecialchars(strip_tags($data->office_lat))));
            $data->client_code = addslashes(strtoupper(htmlspecialchars(strip_tags($data->client_code))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".client 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_client = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'si', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {         
                //$cln_id = mysqli_insert_id($this->dc);
//                $uname = "client".$cln_id;
                $q_user = "update ". DBNAME .".tbl_user set isdeleted=1,ddt=now(),dby='".$_SESSION['user_id']."'  where id_client ='".$data->id."'
                        ";
                //echo $q_user;
                $this->execQuery($q_user);
                                      
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND a.id_client = ". $data->id : null;  
        $qry = "SELECT a.*,b.username,b.id_user FROM ".DBNAME.".client a
        inner join ".DBNAME.".tbl_user b on a.id_client =b.uid and username like 'client%'
        WHERE a.isdeleted = 0
        " . $sql_str ;
        //echo $qry;return false;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
}

$client = new mClient();

switch( $client->a ) {
    
    case "post":
        $client->post();
    break;
    
    case "get":
        $client->get();
    break;   
    
    case "put":
        $client->put();
    break;
    
    case "delete":
        $client->delete();
    break;
}