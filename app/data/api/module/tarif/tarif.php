<?php

class mTarif extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                            
            !empty($data->tarif_name) &&
            !empty($data->tarif_denom) &&
            !empty($data->vehicle_type) &&
            !empty($data->parking_location) 
        ){
            
            $qry = "INSERT INTO ". DBNAME .".tarif 
                    (tarif_name, tarif_denom, id_vehicle_type, id_parking_location, id_client, idt, iby) 
                    VALUES (?, ?, ?, ?, '".$_SESSION['client_id']."', now(), ?)"; 
            //cho $qry;return false;
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssiii',//ssi        
                                   $data->tarif_name, 
                                   $data->tarif_denom, 
                                   $data->vehicle_type, 
                                   $data->parking_location,
                                   $_SESSION['user_id']
                                   );

            // sanitize                                                                                                            
            $data->tarif_name = addslashes((htmlspecialchars(strip_tags($data->tarif_name))));
            $data->tarif_denom = addslashes((htmlspecialchars(strip_tags($data->tarif_denom))));
            $data->vehicle_type = addslashes((htmlspecialchars(strip_tags($data->vehicle_type))));
            $data->parking_location = addslashes((htmlspecialchars(strip_tags($data->parking_location))));                

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created.")); //SAVE_SUCCESS   
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));//SAVE_FAIL
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => SAVE_PARAMS_INVALID));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(                            
            !empty($data->tarif_name) &&
            !empty($data->tarif_denom) &&
            !empty($data->vehicle_type) &&
            !empty($data->parking_location) &&            
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME .".tarif 
                    SET tarif_name= ?, tarif_denom= ?, id_vehicle_type= ?, id_parking_location= ?, uby = ? , udt = now()
                    WHERE id_tarif = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ssiiii',  
                                   $data->tarif_name, 
                                   $data->tarif_denom, 
                                   $data->vehicle_type, 
                                   $data->parking_location,                                   
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                                                 
            $data->tarif_name = addslashes((htmlspecialchars(strip_tags($data->tarif_name))));
            $data->tarif_denom = addslashes((htmlspecialchars(strip_tags($data->tarif_denom))));
            $data->vehicle_type = addslashes((htmlspecialchars(strip_tags($data->vehicle_type))));
            $data->parking_location = addslashes((htmlspecialchars(strip_tags($data->parking_location))));                
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));//EDIT_SUCCESS    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));//EDIT_FAIL
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => EDIT_PARAMS_INVALID));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME .".tarif 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_tarif = ? and id_client='".$_SESSION['client_id']."'";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND id_tarif = ". $data->id : null;  
        $qry = "SELECT a.*,b.vehicle_type,c.parking_location_name FROM ".DBNAME.".tarif a
        inner join vehicle_type b on a.id_vehicle_type=b.id_vehicle_type
        inner join parking_location c on c.id_parking_location=a.id_parking_location
        WHERE a.id_client='".$_SESSION['client_id']."' and a.isdeleted = 0" . $sql_str ;

        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryVehicle = "SELECT * FROM ". DBNAME .".vehicle_type WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        $qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        //$qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
        
        $dataVehicle = $this->selectQueryAsArray($qryVehicle);
        $dataLocation = $this->selectQueryAsArray($qryLocation);
        //$dataKampus = $this->selectQueryAsArray($qryKampus);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataVehicle" => $dataVehicle, "dataLocation" => $dataLocation)); 
    }
    
}

$tarif = new mTarif();

switch( $tarif->a ) {
    
    case "post":
        $tarif->post();
    break;
    
    case "get":
        $tarif->get();
    break;
    
    case "get-form-data":
        $tarif->getFormData();
    break;   
    
    case "put":
        $tarif->put();
    break;
    
    case "delete":
        $tarif->delete();
    break;
}