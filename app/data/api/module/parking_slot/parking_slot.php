<?php

class mParkingSlot extends app {
    
    function __construct() {    
        $this->db_mysql(); 
        $this->getAPIParams(); 
    }       

    function post() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(            
            !empty($data->parking_location) &&
            !empty($data->vehicle_type) &&
            !empty($data->capacity)  
        ){
            
            $qry = "INSERT INTO ". DBNAME.".parking_slot 
                    (id_parking_location,id_vehicle_type, capacity, idt, iby) 
                    VALUES (?, ?, ?, now(), ?)";

            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iisi',
                                   $data->parking_location,                                     
                                   $data->vehicle_type, 
                                   $data->capacity,                                    
                                   $_SESSION['user_id']);

            // sanitize            
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));
            $data->vehicle_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->vehicle_type))));
            $data->capacity = addslashes(strtoupper(htmlspecialchars(strip_tags($data->capacity))));            

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was created."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record."));
            }                       
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to create record. Data is incomplete."));
        }
    }

    function put() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(        
            !empty($data->parking_location) &&
            !empty($data->vehicle_type) &&
            !empty($data->capacity) &&
            !empty($data->id)
        ){            
            $qry = "UPDATE ". DBNAME.".parking_slot 
                    SET id_parking_location= ?,id_vehicle_type= ?, capacity= ?, uby = ? , udt = now()
                    WHERE id_parking_slot = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'iisii', 
                                   $data->parking_location,                                     
                                   $data->vehicle_type, 
                                   $data->capacity,
                                   $_SESSION['user_id'],
                                   $data->id);

            // sanitize                                                         
            $data->parking_location = addslashes(strtoupper(htmlspecialchars(strip_tags($data->parking_location))));
            $data->vehicle_type = addslashes(strtoupper(htmlspecialchars(strip_tags($data->vehicle_type))));
            $data->capacity = addslashes(strtoupper(htmlspecialchars(strip_tags($data->capacity))));
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was updated."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record."));
            }                     
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to update record. Data is incomplete."));
        }
        
    }
    
    function delete() {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
         
        // get posted data
        $data = json_decode(file_get_contents("php://input"));    
        
        if(
            !empty($data->id) 
        ){
                
            $qry = "UPDATE ". DBNAME.".parking_slot 
                    SET isdeleted = 1, dby = ? , ddt = now()
                    WHERE id_parking_slot = ?";
                    
            $stmt = mysqli_prepare($this->dc, $qry);
            mysqli_stmt_bind_param($stmt, 'ii', 
                                   $_SESSION['user_id'], 
                                   $data->id);

            // sanitize    
            $data->id = htmlspecialchars(strip_tags($data->id));

            $exe = mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
            
            if($exe) {                               
                http_response_code(200); // set response code - 201 created 
                echo json_encode(array("status" => OK, "message" => "Record was deleted."));    
            } else {
                
                http_response_code(200); // set response code - 503 service unavailable
                echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record."));
            }      
                   
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to delete record. Data is incomplete."));
        }
    }
    
    function get() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    

        $sql_str = (isset($data->id) && $data->id != '') ? " AND a.id_parking_slot = ". $data->id : null;  
        $qry = "SELECT a.*,b.parking_location_name,c.vehicle_type FROM ".DBNAME.".parking_slot a
        inner join parking_location b on a.id_parking_location=b.id_parking_location
        inner join vehicle_type c on a.id_vehicle_type=c.id_vehicle_type
        WHERE b.id_client='".$_SESSION['client_id']."' and a.isdeleted = 0" . $sql_str ;
        //echo $qry;return false;
        if($this->sqlNumRows($qry) > 0) {
            $numRecords = $this->sqlNumRows($qry);
            $dataRecords = $this->selectQueryAsArray($qry);
            http_response_code(200);
            $this->printJSON(json_encode(array("status" => OK, "numRecords" => $numRecords, "dataRecords" => $dataRecords))); 
                        
        } else {                                     
            http_response_code(200);
            echo json_encode(array("status" => NOT_OK, "message" => "Data tidak ditemukan."));
        }
    }
    
    function getFormData() {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));    
                                                                                             
        $qryVehicle = "SELECT * FROM ". DBNAME .".vehicle_type WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        $qryLocation = "SELECT * FROM ". DBNAME .".parking_location WHERE id_client='".$_SESSION['client_id']."' and isdeleted = 0";
        //$qryTrxtype = "SELECT * FROM ". DBNAME .".trx_type WHERE isdeleted = 0";
        
        $dataVehicle= $this->selectQueryAsArray($qryVehicle);
        $dataLocation = $this->selectQueryAsArray($qryLocation);
        //$dataTrxtype = $this->selectQueryAsArray($qryTrxtype);
        
        http_response_code(200);
        echo json_encode(array("status" => OK, "dataVehicle" => $dataVehicle, "dataLocation" => $dataLocation)); //, "dataTrxtype" => $dataTrxtype
    }
    
}

$parkingslot = new mParkingSlot();

switch( $parkingslot->a ) {
    
    case "post":
        $parkingslot->post();
    break;
    
    case "get":
        $parkingslot->get();
    break;   
    
    case "get-form-data":
        $parkingslot->getFormData();
    break;
    
    case "put":
        $parkingslot->put();
    break;
    
    case "delete":
        $parkingslot->delete();
    break;
}