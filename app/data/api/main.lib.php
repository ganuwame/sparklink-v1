<?php

class main {

    var $dc, $q, $m, $h, $v, $a, $c;          

    function getAPIParams() {

        $this->q = (isset($_REQUEST['q'])) ? (($_REQUEST['q'] != '') ? $_REQUEST['q'] : null) : null; //Keyword
        $this->m = (isset($_REQUEST['m'])) ? (($_REQUEST['m'] != '') ? str_replace("-", "_", $_REQUEST['m']) : null) : null; //Module
        $this->a = (isset($_REQUEST['a'])) ? (($_REQUEST['a'] != '') ? $_REQUEST['a'] : null) : null; //Action Request    
        $this->h = (isset($_REQUEST['h'])) ? (($_REQUEST['h'] != '') ? $_REQUEST['h'] : 0) : null; //Hash Code  

    }

    function db_mysql() {
        $this->dc = mysqli_connect( DBHOST, DBUSER, DBPSWD, DBNAME);
        if( $this->dc === false ) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        mysqli_set_charset($this->dc,"utf8");
    }

    function displayError() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    function getModuleAttribute( $module_identifier ) {
        $qry = "select * from ".DBNAME.".tbl_app_module
                where module_dir = '". $module_identifier. "' or module_hash = '". $module_identifier. "' and isdeleted = 0";
        return $this->selectQueryAsObject( $qry );
    }

    function module( $midentifier ) {
        $mdir = $midentifier; 
        //$mdir = $this->getModuleAttribute($midentifier)->module_dir;
        include "module/" . $mdir . "/" . $mdir . ".php";
    }
        
    function isLogin()
    {
        
        header("Access-Control-Allow-Origin: *");       
        header("Content-Type: application/json; charset=UTF-8");          
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $status = OK;
        
        if(!isset($_SESSION['user_islogin'])) {
            $status = NOT_OK;    
        }
        
        $dataAuth = array();
        if($status == OK) {
            if (strpos($_SESSION['username'], 'jukir') !== false) {
                $dataAuth = array(
                //"status" => OK,   
                "user_id" => $_SESSION['user_id'],                        
                "username" => $_SESSION['username'],
                "name" => $_SESSION['name'],
                "email" => $_SESSION['email'],
                "phone" => $_SESSION['phone'],
                "client_id" => $_SESSION['client_id'],   
                "id_jukir" => $_SESSION['jukir_id'],   
                "jukir_code" => $_SESSION['jukir_code'],   
                "usergroup" => $_SESSION['user_group'],   
                //"userlevel" => $_SESSION['user_level'],
                "usertoken" => $_SESSION['user_token']
                //"vehicle_type_tarif"=> $dataVehicle, 
                //"parking_location"=> $dataLocation
            );
            }elseif (strpos($_SESSION['username'], 'admin') !== false) {
               $dataAuth = array(
                //"status" => OK,   
                "user_id" => $_SESSION['user_id'],                        
                "username" => $_SESSION['username'],
                "name" => $_SESSION['name'],
                "email" => $_SESSION['email'],
                "phone" => $_SESSION['phone'],
                "client_id" => $_SESSION['client_id'],   
                "admin_id" => $_SESSION['admin_id'],   
                //"jukir_code" => $_SESSION['jukir_code'],   
                "usergroup" => $_SESSION['user_group'],   
                //"userlevel" => $_SESSION['user_level'],
                "usertoken" => $_SESSION['user_token']
                //"vehicle_type_tarif"=> $dataVehicle, 
                //"parking_location"=> $dataLocation
                ); 
            }
            else{
               $dataAuth = array(
                //"status" => OK,   
                "user_id" => $_SESSION['user_id'],                        
                "username" => $_SESSION['username'],
                "name" => $_SESSION['name'],
                "email" => $_SESSION['email'],
                "phone" => $_SESSION['phone'],
                "client_id" => $_SESSION['client_id'],   
                //"id_admin" => $_SESSION['admin_id'],   
                //"jukir_code" => $_SESSION['jukir_code'],   
                "usergroup" => $_SESSION['user_group'],   
                //"userlevel" => $_SESSION['user_level'],
                "usertoken" => $_SESSION['user_token']
                //"vehicle_type_tarif"=> $dataVehicle, 
                //"parking_location"=> $dataLocation
                ); 
            }
            
        }
        
        echo json_encode(array("status" => $status, "dataAuth" => $dataAuth));
        
    } 
           
    function getToken()
    {
        
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        
        $data = json_decode(file_get_contents("php://input"));   
        if(
            !empty($data->usr) &&
            !empty($data->pwd)
        ) {
            
            $usr = $data->usr;
            $pwd = $data->pwd;
            
            //$qryUser = "SELECT 
//                            p.client_name, u.* 
//                        FROM ".DBNAME.".tbl_user u                         
//                        INNER JOIN ".DBNAME.".client p ON u.id_client = p.id_client
//                        WHERE u.username = '". $usr ."' AND u.passwd = md5('". $pwd ."')";
            //$a = 'How are you?';
            if (strpos($usr, 'jukir') !== false) {
                $qryUser = "SELECT 
                             u.*,j.jukir_code 
                        FROM ".DBNAME.".tbl_user u 
                        inner join jukir j on j.id_jukir = u.uid                                                
                        WHERE u.username = '". $usr ."' AND u.passwd = md5('". $pwd ."')";
            }elseif(strpos($usr, 'admin') !== false){
                $qryUser = "SELECT 
                             u.* 
                        FROM ".DBNAME.".tbl_user u                                                 
                        WHERE u.username = '". $usr ."' AND u.passwd = md5('". $pwd ."')";
            }else{
                $qryUser = "SELECT 
                             u.* 
                        FROM ".DBNAME.".tbl_user u                                                 
                        WHERE u.username = '". $usr ."' AND u.passwd = md5('". $pwd ."')";    
            }
            
            
            $authUser = $this->sqlNumRows($qryUser);
                                  
            if($authUser > 0) {    
                
                $token = md5($usr . date('Ymdhis'));
                $qry = "UPDATE ".DBNAME.".tbl_user SET token = '". $token ."', tdt = now() 
                WHERE username = '". $usr ."' and isdeleted = 0";
                $exe = $this->execQuery($qry, true); 
                
                $user = json_decode( $this->selectQueryAsJSON($qryUser), true );  
                
                $qryVehicle = "select t.id_tarif,t.tarif_name,t.tarif_denom,t.id_parking_location,t.id_vehicle_type,
                            v.vehicle_type,v.icon_img from ". DBNAME .".tarif t
                            inner join ". DBNAME .".vehicle_type v on v.id_vehicle_type = t.id_vehicle_type
                            WHERE t.id_client='".$user[0]['id_client']."' and t.isdeleted = 0";
                $qryLocation = "SELECT id_parking_location, parking_location_name,parking_location_code 
                FROM ". DBNAME .".parking_location WHERE id_client='".$user[0]['id_client']."' and isdeleted = 0";
                //$qryKampus = "SELECT * FROM ". DBNAME .".tbl_tahfidz_kampus WHERE isdeleted = 0";
                
                $dataVehicle = $this->selectQueryAsArray($qryVehicle);
                $dataLocation = $this->selectQueryAsArray($qryLocation);                
            
                $_SESSION['user_islogin'] = true;
                $_SESSION['user_id'] = trim($user[0]['id_user']);
                $_SESSION['username'] = trim($user[0]['username']);
                $_SESSION['name'] = trim($user[0]['name']);
                $_SESSION['email'] = trim($user[0]['email']);
                $_SESSION['phone'] = trim($user[0]['phone']);
                $_SESSION['client_id'] = $user[0]['id_client'];
                
                if (strpos($usr, 'jukir') !== false) {
                    $_SESSION['jukir_code'] = $user[0]['jukir_code'];
                    $_SESSION['jukir_id'] = $user[0]['uid'];
                }if (strpos($usr, 'admin') !== false) {
                    $_SESSION['admin_code'] = "ADMIN_CODE";
                    $_SESSION['admin_id'] = $user[0]['uid'];
                }
                else{
                    //$_SESSION['admin_code'] = "ADMIN_CODE";
//                    $_SESSION['admin_id'] = $user[0]['uid'];
                }                
                $_SESSION['user_group'] = $user[0]['usergroup'];
                $_SESSION['user_token'] = $token;
            
                if (strpos($usr, 'jukir') !== false) {
                    echo json_encode(
                        array(
                            "status" => OK,   
                            "user_id" => $_SESSION['user_id'],                        
                            "username" => $_SESSION['username'],
                            "name" => $_SESSION['name'],
                            "email" => $_SESSION['email'],
                            "phone" => $_SESSION['phone'],
                            "client_id" => $_SESSION['client_id'],   
                            "id_jukir" => $_SESSION['jukir_id'],   
                            "jukir_code" => $_SESSION['jukir_code'],   
                            "usergroup" => $_SESSION['user_group'],   
                            //"userlevel" => $_SESSION['user_level'],
                            "usertoken" => $_SESSION['user_token'],
                            "vehicle_type_tarif"=> $dataVehicle, 
                            "parking_location"=> $dataLocation
                        )
                    );
                }
                elseif(strpos($usr, 'admin') !== false){
                    echo json_encode(
                        array(
                            "status" => OK,   
                            "user_id" => $_SESSION['user_id'],                        
                            "username" => $_SESSION['username'],
                            "name" => $_SESSION['name'],
                            "email" => $_SESSION['email'],
                            "phone" => $_SESSION['phone'],
                            "client_id" => $_SESSION['client_id'],   
                            "admin_id" => $_SESSION['admin_id'],   
                            //"jukir_code" => $_SESSION['jukir_code'],   
                            "usergroup" => $_SESSION['user_group'],   
                            //"userlevel" => $_SESSION['user_level'],
                            "usertoken" => $_SESSION['user_token'],
                            "vehicle_type_tarif"=> $dataVehicle, 
                            "parking_location"=> $dataLocation
                        )
                    );
                }
                else{
                    echo json_encode(
                        array(
                            "status" => OK,   
                            "user_id" => $_SESSION['user_id'],                        
                            "username" => $_SESSION['username'],
                            "name" => $_SESSION['name'],
                            "email" => $_SESSION['email'],
                            "phone" => $_SESSION['phone'],
                            "client_id" => $_SESSION['client_id'],   
                            //"id_jukir" => $_SESSION['jukir_id'],   
                            //"jukir_code" => $_SESSION['jukir_code'],   
                            "usergroup" => $_SESSION['user_group'],   
                            //"userlevel" => $_SESSION['user_level'],
                            "usertoken" => $_SESSION['user_token'],
                            "vehicle_type_tarif"=> $dataVehicle, 
                            "parking_location"=> $dataLocation
                        )
                    );
                }
                
                
            } else {        
                echo json_encode(array("status" => NOT_OK, "message" => "Ups! Password atau username tidak valid."));
            }
        } else {
            http_response_code(200); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Ups! Password atau username tidak valid."));
            return false;
        }
    }
   
    function tokenAuth()
    {
        $data = json_decode(file_get_contents("php://input"));   
        if(
            !empty($data->usr) &&
            !empty($data->tkn)
        ) {
            
            $usr = $data->usr;
            $tkn = $data->tkn;

            $qry ="SELECT * FROM ".DBNAME.".tbl_user WHERE token = '". $tkn ."' AND username = '". $usr ."'";
            // echo $qry;
            if( $this->sqlNumRows( $qry ) > 0 ) {
                $user = json_decode( $this->selectQueryAsJSON($qry), true );
                $_SESSION['user_islogin'] = true;             
                $_SESSION['user_id'] = trim($user[0]['id_user']);
                $_SESSION['username'] = trim($user[0]['username']);
                $_SESSION['client_id'] = $user[0]['id_client'];
                $_SESSION['user_group'] = $user[0]['usergroup'];
                $_SESSION['user_level'] = $user[0]['userlevel'];

                if($_SESSION['user_islogin'] == true) {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            http_response_code(400); // set response code - 400 bad request
            echo json_encode(array("status" => NOT_OK, "message" => "Unable to authenticate. Data is incomplete."));
        }
    }
    
    function echoMessage( $msg ) {
        die( $msg );
    }

    function logout() {
        $status = NOT_OK;
        
        unset( $_SESSION );
        if(session_destroy()) {    
            $status = OK;                
        }
        
        $ret = json_encode(array("status" => $status));
        $this->printJSON($ret);
    }

    function execQuery( $qry, $displayError=null) {
        if($displayError == true) {
            $exe = mysqli_query( $this->dc, $qry ) or die( print_r( mysqli_error($this->dc), true));
        } else {
            $exe = mysqli_query( $this->dc, $qry ) ;
        }

        return $exe;
    }

    function sqlNumRows( $qry ) {
        $rs = mysqli_query( $this->dc, $qry);
        $num = mysqli_num_rows( $rs );
        return $num;
    }

    function selectQueryAsArray( $qry ) {
        $rs = mysqli_query( $this->dc, $qry ) or die(mysqli_error($this->dc));
        $arrResult = Array();

        while( $rw = mysqli_fetch_assoc( $rs )) {
            array_push( $arrResult, $rw );
        }

        return $arrResult;
    }
    
    function selectQueryAsJSON( $qry ) {
        $rs = mysqli_query( $this->dc, $qry ) or die(print_r(mysqli_error($this->dc), true));
        $arrResult = Array();

        while( $rw = mysqli_fetch_assoc( $rs )) {
            array_push( $arrResult, $rw );
        }

        return json_encode( $arrResult );
    }

    function selectQueryAsObject( $qry ) {
        $rs = mysqli_query( $this->dc, $qry );
        return mysqli_fetch_object( $rs );
    }

    function responseJSON($arrResponse) {
        return json_encode($arrResponse);
    }
    
    function printJSON($jsonData) {
        header('Content-Type: application/json');
        echo $jsonData;
    }

    function dateStringIDFormat($tgl) {
        $strTgl = "";
        if($tgl != "") {
            $xpTgl = explode("-", $tgl);
            if((int)$xpTgl[1] != 0 ) {
                $arrBulan = Array(
                                "Januari",
                                "Februari",
                                "Maret",
                                "April",
                                "Mei",
                                "Juni",
                                "Juli",
                                "Agustus",
                                "September",
                                "Oktober",
                                "November",
                                "Desember",
                            );
                $strTgl = $xpTgl[2]." ".$arrBulan[$xpTgl[1]-1]." ".$xpTgl[0];
            }
        }
        return $strTgl;
    }
}