<?php
    
    $app = new app();

    if($app->q == GET_TOKEN) {
        $app->getToken();
    } elseif($app->q == IS_LOGIN) {
        $app->isLogin(); 
    } elseif($app->q == LOGOUT) {
        $app->logout();
    }
    else {
        if($app->q == 'download-excel') {     
            $app->module($app->m);
        } else {
            if($app->tokenAuth()){
                switch( $app->q ) {
                    case API:
                        $app->module($app->m);
                    break;
                    
                    default:
                        $msg = array("status" => "NOK", "message" => "Oops! Request not found.");
                        $app->printJSON(json_encode($msg));
                    break;
                }
            } else {
                $msg = array("status" => "NOK", "message" => "Oops! Authentication failed.");
                $app->printJSON(json_encode($msg));
            }
        }
    }