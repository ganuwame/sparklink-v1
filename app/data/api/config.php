<?php

date_default_timezone_set('Asia/Jakarta');
session_start(); 

// App Config
define("APP_LANGUAGE", "id");
define("DEBUG_MODE", true);

//Path
define("DOC_UPLOADS_PATH", "doc/uploads/");

// Database Config

//MySQL
define("DBNAME", "u1534565_dbsparklink");
define("DBHOST", "109.106.253.165");//109.106.253.165
define("DBUSER", "u1534565_appsparklink");
define("DBPSWD", "Te\$t12356#1");//Test12356

// Params
define("API", "api");
define("LOGIN", "login");
define("LOGOUT", "logout");
define("TOKEN", "token");
define("GET_TOKEN", "get-token");
define("IS_LOGIN", "is-login");

// Notifikasi  
define("NEED_LOGIN", "Authentication Failed");

//Return Value
define("OK", "OK");
define("NOT_OK", "NOK");
define("SAVE_SUCCESS", "Data berhasil disimpan.");
define("SAVE_FAIL", "Data gagal disimpan.");
define("SAVE_PARAMS_INVALID", "Data gagal disimpan. Data tidak valid.");
define("EDIT_SUCCESS", "Data berhasil diubah.");
define("EDIT_FAIL", "Data gagal diubah.");
define("EDIT_PARAMS_INVALID", "Data gagal diubah. Data tidak valid.");
define("DELETE_SUCCESS", "Data berhasil dihapus.");
define("DELETE_FAIL", "Data gagal dihapus.");
define("DELETE_PARAMS_INVALID", "Data gagal dihapus. Data tidak valid.");
define("DATA_NOT_FOUND", "Data tidak ditemukan.");
define("DATA_NOT_VALID", "Data tidak valid.");
define("UPLOAD_SUCCESS", "Unggah data berhasil.");
define("UPLOAD_FAIL", "Unggah data gagal.");
define("CREATE_GAJI_SUCCESS", "Data gaji berhasil dibuat.");
define("CREATE_GAJI_FAIL", "Data gaji gagal dibuat.");
define("PAID_SUCCESS", "Data pembayaran gaji berhasil disimpan.");
define("PAID_FAIL", "Data pembayaran gaji gagal disimpan.");
define("PAID_PARAMS_INVALID", "Data pembayaran gaji gagal disimpan. Data tidak valid.");