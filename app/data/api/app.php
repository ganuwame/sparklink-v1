<?php

class app extends main {

    function __construct() {

        if(DEBUG_MODE) {
            $this->displayError();
        }
        $this->db_mysql();
        $this->getAPIParams();

    }
    
    function createUpdateStok($stok_penerimaan_id, $jumlah_stok_awal) {    
        $qry = "SELECT stok_penerimaan_id, total_stok_awal, pengeluaran_stok 
                FROM tbl_stok WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";
                                  
        if($this->sqlNumRows($qry) > 0) {
            $qryStok = "UPDATE tbl_stok SET total_stok_awal = ". $jumlah_stok_awal . ", pengeluaran_stok = null, sisa_stok = " . $jumlah_stok_awal .", 
                        udt = now(), uby = '". $_SESSION['user_id'] ."' 
                        WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";                         
        } else {
            $qryStok = "INSERT INTO tbl_stok 
                        SET stok_penerimaan_id = ". $stok_penerimaan_id .", total_stok_awal = ". $jumlah_stok_awal . ", sisa_stok = " . $jumlah_stok_awal .", 
                            puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";
        }
        return $this->execQuery($qryStok);     
    }
    
    function createStokHistori($stok_penerimaan_id, $stok_activity_id, $keterangan_activity, $jumlah_stok_activity) {   
    
        if($stok_activity_id != '1') {
            $qry = "SELECT stok_penerimaan_id, total_stok_awal, pengeluaran_stok, sisa_stok 
                    FROM tbl_stok WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";
            $stok = $this->selectQueryAsObject($qry);
        }
        
        if($stok_activity_id == '1') {
            $qryStok = "INSERT INTO tbl_stok_histori 
                        SET stok_penerimaan_id = ". $stok_penerimaan_id .", stok_activity_id = ". $stok_activity_id .", keterangan_activity = '". $keterangan_activity ."',
                            jumlah_stok_activity = ". $jumlah_stok_activity .", total_stok_awal = ". $jumlah_stok_activity . ", sisa_stok = " . $jumlah_stok_activity .", 
                            puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";
        } elseif($stok_activity_id == '2') {                                                    
            $qryStok = "INSERT INTO tbl_stok_histori 
                        SET stok_penerimaan_id = ". $stok_penerimaan_id .", stok_activity_id = ". $stok_activity_id .", keterangan_activity = '". $keterangan_activity ."', 
                            puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";  
        } elseif($stok_activity_id == '3') {
            $qryStok = "INSERT INTO tbl_stok_histori 
                        SET stok_penerimaan_id = ". $stok_penerimaan_id .", stok_activity_id = ". $stok_activity_id .", keterangan_activity = '". $keterangan_activity ."',
                            jumlah_stok_activity = ". $jumlah_stok_activity .", total_stok_awal = ". $stok->total_stok_awal . ", 
                            total_pengeluaran_stok = ". $stok->pengeluaran_stok .", sisa_stok = " . $stok->sisa_stok .", 
                            puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";
        } elseif($stok_activity_id == '4') {
            $qryStok = "INSERT INTO tbl_stok_histori 
                        SET stok_penerimaan_id = ". $stok_penerimaan_id .", stok_activity_id = ". $stok_activity_id .", keterangan_activity = '". $keterangan_activity ."',
                            jumlah_stok_activity = ". $jumlah_stok_activity .", total_stok_awal = ". $stok->total_stok_awal . ", 
                            total_pengeluaran_stok = ". $stok->pengeluaran_stok .", sisa_stok = " . $stok->sisa_stok .", 
                            puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";
        } 

        return $this->execQuery($qryStok);     
    }
    
    function deleteStok($stok_penerimaan_id) {    
        $qry = "SELECT stok_penerimaan_id, total_stok_awal, pengeluaran_stok 
                FROM tbl_stok WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";
                                  
        if($this->sqlNumRows($qry) > 0) {
            $qryStok = "UPDATE tbl_stok SET isdeleted = 1,  
                        ddt = now(), dby = '". $_SESSION['user_id'] ."' 
                        WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";                         
        }
        return $this->execQuery($qryStok);     
    }
    
    function updatePengeluaranStok($stok_penerimaan_id, $jumlah_pengeluaran_stok) {                                
        
        $qry = "SELECT stok_penerimaan_id, total_stok_awal, pengeluaran_stok 
                FROM tbl_stok WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";
                                  
        if($this->sqlNumRows($qry) > 0) {
            
            $stok = $this->selectQueryAsObject($qry);
            $pengeluaran_stok = $stok->pengeluaran_stok + $jumlah_pengeluaran_stok;
            $sisa_stok = $stok->total_stok_awal - $pengeluaran_stok;
            
            $qryStok = "UPDATE tbl_stok SET pengeluaran_stok = ". $pengeluaran_stok . ", sisa_stok = " . $sisa_stok .", udt = now(), uby = '". $_SESSION['user_id'] ."' 
                        WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0"; 
        } else {
            $qryStok = "INSERT INTO tbl_stok SET stok_penerimaan_id = ". $stok_penerimaan_id .", total_stok_awal = ". $jumlah_stok_update . ", sisa_stok = " . $jumlah_stok_update .", 
                        puskesmas_id = '". $_SESSION['puskesmas_id'] ."', idt = now(), iby = '". $_SESSION['user_id'] ."'";
        } 
        
        return $this->execQuery($qryStok);     
    }
    
    function stokIsNotUsed($stok_penerimaan_id) {
        $qry = "select stok_penerimaan_id FROM tbl_stok_pengeluaran WHERE stok_penerimaan_id = ". $stok_penerimaan_id ." AND isdeleted = 0";
        if($this->sqlNumRows($qry) > 0) {
            return false;
        } else {
            return true;
        }
    } 
}